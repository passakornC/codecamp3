package com.codecamp.day10.my_interface;

public interface ICockroachKiller {
    public void buyBaygon(int number);
    public void killCockroach(int baygonNumber, String building, String roomName);
    public int getTotalKilled();
}
   