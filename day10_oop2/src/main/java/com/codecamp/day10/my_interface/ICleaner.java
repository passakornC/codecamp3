package com.codecamp.day10.my_interface;

import com.codecamp.day10.Tools;

import java.util.ArrayList;
import java.util.HashMap;

public interface ICleaner {
    public void setTools(Tools toolName);
    public void clean(String building, String roomName);
//    public String[] getCleanedRoom();
    public ArrayList<HashMap<String, String>> getCleanedRoom();
}
   