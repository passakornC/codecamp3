package com.codecamp.day10.my_interface;

public interface IWebsiteCreator {
    public void createWebsite(String template, String titleName);
}
   