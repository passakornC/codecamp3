package com.codecamp.day10;

public class Employee {
    public String firstname;
    private String lastname;
    private int salary;
    private String id;
    private String dressCode;

    public Employee(String firstnameInput, String lastnameInput, int salaryInput) {
        this.firstname = firstnameInput;
        this.lastname = lastnameInput;
        this.salary = salaryInput;
    }

    public Employee(String firstname, String lastname, int salary, String id) {
        this(firstname, lastname, salary);
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setSalary(int newSalary) {

    }

    public int getSalary() {
        return salary;
    }

    public void hello() {
        System.out.println("Hello " + this.firstname);
    }

    public void work() {
        this.work();
    }
}
   
   