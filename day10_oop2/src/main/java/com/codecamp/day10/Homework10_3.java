package com.codecamp.day10;

public class Homework10_3 {
    private static String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend role:Programmer",
            "id:1002 firstname:Tony lastname:Stark salary:20000 type:tshirt role:CEO",
            "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack role:Programmer",
            "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:maid role:OfficeCleaner"
    };

    public static void main(String[] args) {
        CEO ceo = new CEO("CeoFirstname", "CeoLastname", 10000, "1001", "suit");

        for (String employee : Homework10_3.rawData) {
            String role = employee.split("role:")[1];
            String[] splitString = employee.split(" ");
            String id = "";
            String firstname = "";
            String lastname = "";
            int salary = 0;
            String typeOrDresscode = "";

            for (String keyAndValue : splitString) {
                String key = keyAndValue.split(":")[0].toLowerCase();
                String value = keyAndValue.split(":")[1];

                if (!key.equals("role")) {
                    if (key.equals("id")) {
                        id = value;
                    } else if (key.equals("firstname")) {
                        firstname = value;
                    } else if (key.equals("lastname")) {
                        lastname = value;
                    } else if (key.equals("salary")) {
                        salary = Integer.parseInt(value);
                    } else {
                        typeOrDresscode = value;
                    }
                }
            }

            if (role.equals("Programmer")) {
                ceo.setEmployees(new Programmer(firstname, lastname, salary, id, typeOrDresscode));
            } else if(role.equals("CEO")) {
                ceo.setEmployees(new CEO(firstname, lastname, salary, id, typeOrDresscode));
            } else {
                ceo.setEmployees(new OfficeCleaner(firstname, lastname, salary, id, typeOrDresscode));
            }
        }

//        Display
        for (Employee employee : ceo.getEmployees()) {
            System.out.println(employee.getFirstname());
            employee.work();
            System.out.println("==================================================");
        }
    }
}
