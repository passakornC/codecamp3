package com.codecamp.day10;

import com.codecamp.day10.my_interface.ICleaner;
import com.codecamp.day10.my_interface.ICockroachKiller;

import java.util.ArrayList;
import java.util.HashMap;

public class OfficeCleaner extends Employee implements ICockroachKiller, ICleaner {
    /*
     * Property
     */
    private String dressCode;

    private Tools toolName;
    private static int numberOfKilled;
    private static ArrayList<HashMap<String, String>> cockroachKilledList;


    /*
     * Constructor
     */
    public OfficeCleaner(String firstname, String lastname, int salary) {
        this(firstname, lastname, salary, "", "");
    }

    public OfficeCleaner(String firstname, String lastname, int salary, String id, String dressCode) {
        super(firstname, lastname, salary, id);
        this.dressCode = dressCode;
        OfficeCleaner.numberOfKilled = 0;
        OfficeCleaner.cockroachKilledList = new ArrayList<>();
    }


    /*
     * Method
     */
    public static void decorateRoom() {
        System.out.println("DocorateRoom");
    }

    public static void welcomeGuest() {
        System.out.println("WelcomeGuest");
    }

    public void work() {
        this.clean("Building A", "Room A");
        this.killCockroach(99, "Building A", "Room A");
        OfficeCleaner.decorateRoom();
        OfficeCleaner.welcomeGuest();
    }


    /*
     * Method from ICockroachKiller
     */
    public void buyBaygon(int number) {
    }

    public int getTotalKilled() {
        return OfficeCleaner.numberOfKilled;
    }

    public void killCockroach(int number, String building, String roomName) {
        OfficeCleaner.numberOfKilled += 100;
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("building", building);
        hashMap.put("roomName", roomName);
        OfficeCleaner.cockroachKilledList.add(hashMap);
        System.out.println("Office cleaner uses " + number + " baygons to kill cockroaches in " + roomName + " at " + building);
    }


    /*
     * Method from ICleaner
     */
    public void setTools(Tools toolName) {
        this.toolName = toolName;
    }

    public void clean(String building, String roomName) {
        System.out.println("Office cleaner cleans at " + roomName + " in " + building);
    }

    public ArrayList<HashMap<String, String>> getCleanedRoom() {
        return OfficeCleaner.cockroachKilledList;
    }
}
