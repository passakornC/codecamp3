package com.codecamp.day10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.codecamp.day10.my_interface.IWebsiteCreator;
import com.codecamp.day10.my_interface.ICleaner;

public class Homework10_2 {
    public static void main(String[] args) {
        CEO ceo = new CEO("Captain", "Marvel", 30000);
        Programmer prog = new Programmer("Dang", "Red", 20000);
        OfficeCleaner cleaner = new OfficeCleaner ("Somsri", "Sudsuay", 10000);
        AI alphaGo = new AI("alphaGo", "Java");

        ceo.orderWebsite(prog);
        System.out.println(cleaner.getTotalKilled());
        System.out.println(alphaGo.getTotalKilled());
        ceo.orderKillCockroach(cleaner);
        ceo.orderKillCockroach(alphaGo);
        ceo.orderKillCockroach(alphaGo);
        System.out.println(cleaner.getTotalKilled());
        System.out.println(alphaGo.getTotalKilled());
        ArrayList<HashMap<String, String>> resultByHuman = cleaner.getCleanedRoom();
        ArrayList<HashMap<String, String>> resultByAI = alphaGo.getCleanedRoom();
        System.out.println(resultByHuman.get(0));
        System.out.println(resultByAI.get(0));
        System.out.println(resultByAI.get(1));

        ceo.orderCleaned(cleaner);
        ceo.orderCleaned(alphaGo);

        alphaGo.setTools(Tools.MOP);
        cleaner.setTools(Tools.HANDKERCHIEF);
    }
}