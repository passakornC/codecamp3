package com.codecamp.day10;

import com.codecamp.day10.my_interface.ICleaner;
import com.codecamp.day10.my_interface.ICockroachKiller;
import com.codecamp.day10.my_interface.IWebsiteCreator;

import java.util.ArrayList;

public class CEO extends Employee {
    /*
     * Property
     */
    private ArrayList<Employee> employees;
    private String dressCode;


    /*
     * Constructor
     */
    public CEO(String firstname, String lastname) {
        this(firstname, lastname, 10000);
    }

    public CEO(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
    }

    public CEO(String firstname, String lastname, int salary, String id, String dressCode) {
        super(firstname, lastname, salary, id);
        this.dressCode = dressCode;
        this.employees = new ArrayList<>();
    }

    @Override
    public int getSalary() {
        return super.getSalary() * 2;
    }
    
    public void hello() {
        System.out.println("Hi, nice to meet you. "+this.firstname+"!");
    }

    public void orderWebsite(IWebsiteCreator creator) {
        String template = "Bootstrap";
        String title = "My website";
        creator.createWebsite(template, title);
    }

    public void orderKillCockroach(ICockroachKiller cockroachKiller) {
        int number = 250;
        String building = "Building A";
        String roomName = "Room A";
        cockroachKiller.killCockroach(number, building, roomName);
    }

    public void orderCleaned(ICleaner cleaner) {
        String building = "Building Z";
        String roomName = "Room Z";
        cleaner.clean(building, roomName);
    }

    public void setEmployees(Employee employee) {
        this.employees.add(employee);
    }

    public ArrayList<Employee> getEmployees() {
        return this.employees;
    }

    public void fire() {
//        this.dressCode = "tshirt";
//        return employee.getFirstname() + " has been fired! Dress with :" + this.dressCode;
        System.out.println("fire()");
    }

    public void hire() {
//        this.dressCode = "tshirt";
//        return employee.getFirstname() + " has been hired back! Dress with :" + this.dressCode;
        System.out.println("hire()");
    }

    public void seminar() {
//        this.dressCode = "suit";
//        return "He is going to seminar. Dress with :" + this.dressCode;
        System.out.println("seminar()");
    }

    public void golf () {
//        this.dressCode = "golf_dress";
//        return "He goes to golf club to find a new connection. Dress with :" + this.dressCode;
        System.out.println("golf");
    }

    public void work() {
        this.seminar();
        this.hire();
        this.fire();
        this.golf();
    }
}
   
   