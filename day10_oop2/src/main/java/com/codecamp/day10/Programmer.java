package com.codecamp.day10;

import com.codecamp.day10.my_interface.IWebsiteCreator;
import com.codecamp.day10.my_interface.IWindowsInstaller;

public class Programmer extends Employee implements IWebsiteCreator, IWindowsInstaller {
    private String type;

    public Programmer(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
    }

    public Programmer(String firstname, String lastname, int salary, String id, String type) {
        super(firstname, lastname, salary, id);
        this.type = type;
    }

    public void fixPC(String serialNumber) {
        System.out.println("I'm trying to fix PC serialNumber:" + serialNumber);
    }

    // สร้าง Method createWebsite()
    public void createWebsite(String template, String titleName) {
        System.out.println("Programmer creates a website by using template: " + template + " and title: " + titleName);
    }
    // สร้าง Method installWindows()
    public void formatWindows(String drive) {
        System.out.println("Format drive " + drive + " successfully.");
    }

    public void installWindows(String version, String productKey) {
        System.out.println("Installs windows " + version + " using product key: [" + productKey + "]");
    }
    public int getLastInstalledWindowsVersion() {
        return 10;
    }

    public void work() {
        this.createWebsite("Bootstrap", "My website");
        this.fixPC("1234");
        this.installWindows("10", "11223344");
    }
}
   
   