package com.codecamp.day10;

import com.codecamp.day10.my_interface.ICockroachKiller;
import com.codecamp.day10.my_interface.ICleaner;
import com.codecamp.day10.my_interface.IWebsiteCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;

public class AI implements ICockroachKiller, ICleaner, IWebsiteCreator {
    /*
     * Property
     */
    private String name;
    private String language;
    private int number;
    private int baygonNumber;
    private String building;
    private String[] cleanedRoomName;
    private Tools toolName;
    private static int numberOfKilled;
    private static ArrayList<HashMap<String, String>> cockroachKilledList;


    /*
     * Constructor
     */
    public AI() {
        AI.numberOfKilled = 0;
        AI.cockroachKilledList = new ArrayList<>();
    }

    public AI(String name, String language) {
        this();
        this.name = name;
        this.language =language;
    }


    /*
     * Method from ICockroachKiller
     */
    public void buyBaygon(int number) {
        System.out.println("Buy " + number + " baygon(s).");
    }

    public void killCockroach(int baygonNumber, String building, String roomName) {
        AI.numberOfKilled += 100;
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("building", building);
        hashMap.put("roomName", roomName);
        AI.cockroachKilledList.add(hashMap);
        System.out.println("AI uses " + baygonNumber + " to kill cockroach in " + roomName + " of " + building);
    }

    public int getTotalKilled() {
        return AI.numberOfKilled;
    }


    /*
     * Method from ICleaner
     */
    public void setTools(Tools toolName) {
        this.toolName = toolName;
    }

    public void clean(String building, String roomName) {
        System.out.println("AI cleans at " + roomName + " in " + building);
    }

    public ArrayList<HashMap<String, String>> getCleanedRoom() {
        return AI.cockroachKilledList;
    }


    /*
     * Method from IWebsiteCreator
     */
    public void createWebsite(String template, String titleName) {
        System.out.println("AI is creating a website by using " + template + " and title is " + titleName);
    }
}
   
   