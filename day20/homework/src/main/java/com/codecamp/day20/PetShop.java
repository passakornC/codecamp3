package com.codecamp.day20;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PetShop {
    public static void main(String[] args) {
        try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Homework1.xml")) {
            // tiger
            Animal tiger = (Animal) context.getBean("tiger");
            System.out.println(tiger.getName());

            // cat
            Animal cat = (Animal) context.getBean("cat");
            System.out.println(cat.getName());
        }
    }
}
