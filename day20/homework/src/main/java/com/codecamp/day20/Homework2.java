package com.codecamp.day20;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Homework2 {
    public DatabaseService databaseService;

    public Homework2(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public DatabaseService getDatabaseService() {
        return databaseService;
    }

    public void setDatabaseService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public void printAllInstructors() {
        this.databaseService.printAllInstructors();
    }

    public static void main(String[] args) {
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("DataSource.xml")) {
            Homework2 homework2 = (Homework2) context.getBean("homework2");
            homework2.printAllInstructors();
        }
    }
}
