<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-04-10
  Time: 14:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        .container {
            max-width: 600px;
        }
    </style>
</head>
<body>

<form:form method="POST" action="/day22homework/registerhw2" cssClass="d-flex justify-content-center align-items-center"
           cssStyle="height: 100%">

    <div class="container" style="position: relative;">
        <c:if test="${message != null}">
            <div style="position: absolute; top: -4em; left: 220px;" class="alert alert-danger" role="alert">
                ${message}
            </div>
        </c:if>

        <div class="row">
            <div class="col">
                <h4>Register Form</h4>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-6">
                <label for="firstname">Firstname</label>
                <input type="text" class="form-control" name="firstname" id="firstname" placeholder="firstname">
            </div>
            <div class="form-group col-6">
                <label for="lastname">Lastname</label>
                <input type="text" class="form-control" name="lastname" id="lastname" placeholder="lastname">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="email@example.com">
            </div>
        </div>

        <button style="display: block; margin: 0 auto;" type="submit" class="btn btn-primary">Submit</button>
    </div>
</form:form>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
