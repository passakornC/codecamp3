<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-04-10
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <title>Submitted Student Information</title>
</head>
<body>
<h2>Submitted Student Information</h2>
<table>
    <tr>
        <td>First firstname: </td>
        <td>${firstname}</td>
    </tr>
    <tr>
        <td>Last firstname: </td>
        <td>${lastname}</td>
    </tr>
    <tr>
        <td>Email: </td>
        <td>${email}</td>
    </tr>
</table>
</body>
</html>
