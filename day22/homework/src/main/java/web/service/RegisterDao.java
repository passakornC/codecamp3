package web.service;

import com.mysql.cj.jdbc.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

public class RegisterDao {
    private Connection connection;
    private static RegisterDao instance = null;

    private RegisterDao(String url, String user, String password, String database) {
        try {
            DriverManager.registerDriver(new Driver());
            connection = DriverManager.getConnection("jdbc:mysql://" + url + "/" + database, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static RegisterDao getInstance() {
        if (Objects.isNull(instance)) {
            instance = new RegisterDao("localhost:3306", "root", "CodeC@mp3", "day22");
        }

        return instance;
    }

    public ArrayList<String> getEmailByName(String firstname, String lastname) {
        ArrayList<String> emailArrayList = new ArrayList<>();

        String[] bindValue = new String[] {firstname, lastname}; // bindValue = {firstname, lastname}
        String sql = "select * from student where firstname = ? and lastname = ?";
//
//        // create array
//        // 1
//        String[][] bindValue1 = new String[5][5]; // array 2 dimension, no initialization
//        // 2
//        String[][] bindValue2 = new String[][] {{"String-value1", "value2"},
//                                                {"value3", "value4"}}; // array 1 dimension, initialization
//        ArrayList<String> arrayList = new ArrayList<>();



//        // 1 Statement
//        try {
//            Statement statement = connection.createStatement();
//
//            ResultSet resultSet = statement.executeQuery("select * from student where firstname = 'TestFirstname'");
//            while (resultSet.next()) { ... }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        // 2 PrepareStatement
//        PreparedStatement ps = connection.prepareStatement("select * from student where firstname = ? and lastname = ?");
//        ps.setString(1, firstname);
//        ps.setString(2, lastname);
//
//        for (int i = 0; i < bindValue.length; i++) {
//            ps.setString(i + 1, bindValue[i]);
//        }
//        ResultSet resultSet = ps.executeQuery();
//        while (resultSet.next()) {}

        try {
            // statement
            PreparedStatement ps = connection.prepareStatement(sql);
            for (int i = 0; i < bindValue.length; i++) {
                ps.setString(i + 1, bindValue[i]);
            }

            // result set
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                emailArrayList.add(resultSet.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return emailArrayList;
    }
}
