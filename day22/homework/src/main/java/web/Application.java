package web;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import web.controller.RegisterController;
import web.service.RegisterDao;

import java.io.File;
import java.util.ArrayList;

public class Application {
    public static void main(String[] args) throws LifecycleException {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir("temp");
        tomcat.setPort(8090);
        tomcat.addWebapp("/day22homework", new File("src/main/webapp").getAbsolutePath());
        tomcat.getConnector();
        tomcat.start();
    }
}
