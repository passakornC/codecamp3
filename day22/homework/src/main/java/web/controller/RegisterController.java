package web.controller;

import com.mysql.cj.jdbc.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import web.domain.Student;
import web.service.RegisterDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

@Controller
public class RegisterController {
    private Student student;

    public Student getStudent() {
        return student;
    }

    @Autowired
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     * Day22 Homework1
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView student() {
        return new ModelAndView("registerForm", "command", new Student());
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String addStudent(@ModelAttribute("SpringWeb") Student student, ModelMap model) {
        if ((Objects.isNull(student.getFirstname()) || (student.getFirstname().equals(""))) ||
                (Objects.isNull(student.getLastname()) || (student.getLastname().equals(""))) ||
                (Objects.isNull(student.getEmail()) || (student.getEmail().equals("")))) {
            model.addAttribute("message", "All fields are require.");
        } else {
            if (student.getEmail().equals(getStudent().getEmail())) {
                model.addAttribute("message", student.getEmail() + " is already existing.");
            } else {
                model.addAttribute("firstname", student.getFirstname());
                model.addAttribute("lastname", student.getLastname());
                model.addAttribute("email", student.getEmail());
                return "result";
            }
        }
        return "registerForm";
    }


    /**
     * Day22 Homework2
     */
    @RequestMapping(value = "/registerhw2", method = RequestMethod.GET)
    public ModelAndView studenthw2() {
        return new ModelAndView("registerFormHw2", "command", new Student());
    }

    @RequestMapping(value = "/registerhw2", method = RequestMethod.POST)
    public String checkStudent(@ModelAttribute("SpringWeb") Student student, ModelMap model) {
        if (isInputEmpty(student.getFirstname(), student.getLastname(), student.getEmail())) { // check for an empty input
            model.addAttribute("message", "All fields are required.");
            return "registerFormHw2";
        } else {
            RegisterDao registerDao = RegisterDao.getInstance();
            ArrayList<String> emailArrayList = registerDao.getEmailByName(student.getFirstname(), student.getLastname());


            for (String email : emailArrayList) {
                if (student.getEmail().equals(email)) {
                    model.addAttribute("message", student.getEmail() + " is already existing.");
                    return "registerFormHw2";
                }
            }

            model.addAttribute("firstname", student.getFirstname());
            model.addAttribute("lastname", student.getLastname());
            model.addAttribute("email", student.getEmail());
            return "result";
        }
    }

    private boolean isInputEmpty(String firstname, String lastname, String email) {
        return ((Objects.isNull(firstname) || (firstname.isEmpty()) ||
                (Objects.isNull(lastname) || (lastname.isEmpty())) ||
                (Objects.isNull(email) || (email.isEmpty()))));
    }
}



