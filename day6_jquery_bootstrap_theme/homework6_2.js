$(() => {
    const apiKey = '10e0ab6315f01a5647be';
    let url = `https://free.currencyconverterapi.com/api/v6/currencies?apiKey=${apiKey}`;
    let fromCurrencySelect = $('#from-currency-list');
    let toCurrencySelect = $('#to-currency-list');

    $.get(url, response => {
        for (let currency in response[Object.keys(response)]) {
            if (currency !== 'ALL') {
                if (currency === 'EUR') {
                    fromCurrencySelect.append(`<option value=${currency} selected="selected">${currency}</option>`);
                } else if (currency === 'THB') {
                    toCurrencySelect.append(`<option value=${currency} selected="selected">${currency}</option>`);
                } else {
                    fromCurrencySelect.append(`<option value=${currency}>${currency}</option>`);
                    toCurrencySelect.append(`<option value=${currency}>${currency}</option>`);
                }
            }
        }
    });
});


$('#convert-button').click(() => {
    const apiKey = '10e0ab6315f01a5647be';
    let amount = parseFloat($('#from-currency').val());
    let outputToCurrency = $('#to-currency');
    let fromCurrency = $('#from-currency-list').val();
    let toCurrency = $('#to-currency-list').val();

    fromCurrency = encodeURIComponent(fromCurrency);
    toCurrency = encodeURIComponent(toCurrency);
    let query = fromCurrency + '_' + toCurrency;

    let url = `http://free.currencyconverterapi.com/api/v5/convert?q=${query}&compact=y&apiKey=${apiKey}`;

    $.get(url, response => {
        let exchangeRate = response[Object.keys(response)]['val'];
        amount = amount * exchangeRate;
        outputToCurrency.val(amount);
    });
});

$('#to-currency').on('change keyup', () => {
    const apiKey = '10e0ab6315f01a5647be';
    let amount = parseFloat($('#to-currency').val());
    let fromCurrency = $('#from-currency-list').val();
    let toCurrency = $('#to-currency-list').val();
    let inputFromCurrency = $('#from-currency');

    fromCurrency = encodeURIComponent(fromCurrency);
    toCurrency = encodeURIComponent(toCurrency);
    let query = toCurrency + '_' + fromCurrency;

    let url = `http://free.currencyconverterapi.com/api/v5/convert?q=${query}&compact=y&apiKey=${apiKey}`;

    $.get(url, response => {
        let exchangeRate = response[Object.keys(response)]['val'];
        amount = amount * exchangeRate;
        inputFromCurrency.val(amount);
    });
});
