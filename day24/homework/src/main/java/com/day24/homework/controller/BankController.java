package com.day24.homework.controller;

import com.day24.homework.domain.BankAccountRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class BankController {

    @PostMapping(path = "/camp3/api/day24/hw1/openaccount", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<HashMap<String, String>> openAccountHw1(@RequestBody BankAccountRequest bankAccountRequest) {
        return ResponseEntity.ok().body(createResponseBody(bankAccountRequest));
    }

    @PostMapping(path = "/camp3/api/day24/hw1/closeaccount", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<HashMap<String, String>> closeAccountHw1(@RequestBody BankAccountRequest bankAccountRequest) {
        HashMap<String, String> hashMap = createResponseBody(bankAccountRequest);
        hashMap.replace("account_isActive", "0");
        hashMap.replace("balance", "0.00");
        return ResponseEntity.ok().body(hashMap);
    }

    @PostMapping(path = "/camp3/api/day24/hw1/deposit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<HashMap<String, String>> depositHw1(@RequestBody BankAccountRequest bankAccountRequest) {
        HashMap<String, String> hashMap = createResponseBody(bankAccountRequest);
        hashMap.replace("balance", bankAccountRequest.getAmount().add(bankAccountRequest.getAmountToDepositOrWithdraw()).toString());
        return ResponseEntity.ok().body(hashMap);
    }

    @PostMapping(path = "/camp3/api/day24/hw1/withdraw", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<HashMap<String, String>> withdrawHw1(@RequestBody BankAccountRequest bankAccountRequest) {
        HashMap<String, String> hashMap = createResponseBody(bankAccountRequest);
        hashMap.replace("balance", bankAccountRequest.getAmount().subtract(bankAccountRequest.getAmountToDepositOrWithdraw()).toString());
        return ResponseEntity.ok().body(hashMap);
    }

    private HashMap<String, String> createResponseBody(BankAccountRequest bankAccountRequest) {
        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("personId", bankAccountRequest.getPersonId());
        hashMap.put("personFullName", bankAccountRequest.getPersonFullName());
        hashMap.put("person_isActive", bankAccountRequest.getPersonStatus().toString());
        hashMap.put("accountNo", bankAccountRequest.getAccountNo());
        hashMap.put("accountType", bankAccountRequest.getAccountType());
        hashMap.put("balance", bankAccountRequest.getAmount().toString());
        hashMap.put("account_isActive", bankAccountRequest.getAccountStatus().toString());

        return hashMap;
    }

}
