package com.day24.homework.domain;

import java.math.BigDecimal;

public class BankAccount {
    private String accountNo;
    private AccountType accountType;
    private BigDecimal balance;
    private boolean isActive;
}
