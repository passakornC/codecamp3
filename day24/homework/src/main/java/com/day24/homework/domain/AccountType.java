package com.day24.homework.domain;

public enum AccountType {
    SAVING,
    CURRENT
}
