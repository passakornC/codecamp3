package com.day24.homework.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonNaming(value = PropertyNamingStrategy.KebabCaseStrategy.class)
public class Person {
    private String personId;
    private String personFullName;
    private boolean isActive;
}
