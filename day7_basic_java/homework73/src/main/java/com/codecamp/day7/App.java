package com.codecamp.day7;

public class App 
{
    public static void main( String[] args ) {
        App app = new App();

        for (int number = 2; number <= 4; number++) {
            System.out.println("Number: " + number);

            app.draw17(number);

            System.out.println();
            System.out.println();
        }
    }

    private void draw9(int number) {
        for (int row = 0; row < number; row++) {
            System.out.println(row * 2);
        }
    }

    private void draw10(int number) {
        for (int row = 0; row < number; row++) {
            System.out.println((row + 1) * 2);
        }
    }

    private void draw11(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print((row + 1) * (col + 1));
            }
            System.out.println();
        }
    }

    private void draw12(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                if (row == col) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw13(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                if (col == (number - (row + 1))) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw14(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                if (col > row) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw15(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                if (col < (number - row)) {
                    System.out.print('*');
                } else {
                    System.out.print('-');
                }
            }
            System.out.println();
        }
    }

    private void draw16(int number) {
        for (int row = 0; row < number * 2 - 1; row++) {
            for (int col = 0; col < number; col++) {
                if (col > Math.abs((number - 1) - Math.abs((number - 1) - row))) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw17(int number) {
        for (int row = 0; row < number * 2 - 1; row++) {
            int threshold = Math.abs((number - 1) - Math.abs((number - 1) - row));
            for (int col = 0; col < number; col++) {
                if (col > threshold) {
                    System.out.print('-');
                } else {
                    System.out.print(threshold + 1);
                }
            }
            System.out.println();
        }
    }
}


