package com.codecamp.day7;

public class App
{
    public static void main( String[] args )
    {
        App app = new App();
        app.hw2();
    }

    private void hw2() {
        String[][] table = {
                { "1", "2", "3" },
                { "4", "5", "6" },
                { "7", "8", "9" }
        };

        for (String[] row : table) {
            int countColumn = 0;

            for (String stringNumber : row) {
                System.out.print(Integer.parseInt(stringNumber) * 2);
                if (countColumn++ != row.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }
}
