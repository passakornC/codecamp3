package com.codecamp.day7;

import java.util.Scanner;

public class App {
    public static void main( String[] args ) {
        Scanner scanner = new Scanner(System.in);

        System.out.println( "Enter a number: " );
        int number = scanner.nextInt();
        scanner.close();

        for (int i = 1; i <= 12; i++) {
            System.out.println(number + " X " + i + " = " + (number * i));
        }
    }
}
