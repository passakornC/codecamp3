package com.codecamp.day7;

public class App {
    public static void main( String[] args ) {
        App app = new App();

        for (int i = 2; i <= 4; i++) {
            System.out.println("Number: " + i);

            app.draw25(i);

            System.out.println();
            System.out.println();
        }

    }

    private void draw18(int number) {
        for (int outIndex = 0; outIndex < number; outIndex++) {
            int threshold;

            for (int inIndex = 0; inIndex < number; inIndex++) {
                threshold = number - (outIndex + 1);
                if (outIndex < inIndex) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw19(int number) {
        for (int outIndex = 0; outIndex < number; outIndex++) {
            for (int inIndex = 0; inIndex < number; inIndex++) {
                if (inIndex < outIndex) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw20(int number) {
        for (int outIndex = 0; outIndex < (number * 2 - 1); outIndex++) {
            for (int inIndex = 0; inIndex < number; inIndex++) {
                if (inIndex < number - (outIndex + 1)) {
                    System.out.print('-');
                } else if (inIndex < (outIndex - (number - 1))) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw21(int number) {
        int count = 1;
        for (int outIndex = 0; outIndex < (number * 2 - 1); outIndex++) {
            for (int inIndex = 0; inIndex < number; inIndex++) {
                if (inIndex < number - (outIndex + 1)) {
                    System.out.print('-');
                } else if (inIndex < (outIndex - (number - 1))) {
                    System.out.print('-');
                } else {
                    System.out.print(count++);
                }
            }
            System.out.println();
        }
    }

    private void draw22(int number) {
        int mediumNumber = number - 1;

        for (int row = 0; row < number; row++) {
            for (int col = 0; col < (number * 2 - 1); col++) {
                if ((col < (mediumNumber - row)) || (col > (mediumNumber + row))) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw23(int number) {
        int mediumNumber = number - 1;
        int difference;

        for (int row = 0; row < number; row++) {
            difference = number - (row + 1);
            for (int col = 0; col < (number * 2 - 1); col++) {
                if ((col < (mediumNumber - difference)) || (col > (mediumNumber + difference))) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw24(int number) {
        int difference;
        int mediumNumber = number - 1;

        for (int row = 0; row < (number * 2 - 1); row++) {
            if (row < mediumNumber || row > mediumNumber) {
                difference = mediumNumber - Math.abs(row - mediumNumber);
            } else {
                difference = mediumNumber;
            }

            for (int col = 0; col < (number * 2 - 1); col++) {
                if ((col < (mediumNumber - difference)) || (col > (mediumNumber + difference))) {
                    System.out.print('-');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }

    private void draw25(int number) {
        int difference;
        int count = 1;
        int mediumNumber = number - 1;

        for (int row = 0; row < (number * 2 - 1); row++) {
            if (row < mediumNumber || row > mediumNumber) {
                difference = mediumNumber - Math.abs(row - mediumNumber);
            } else {
                difference = mediumNumber;
            }

            for (int col = 0; col < (number * 2 - 1); col++) {
                if ((col < (mediumNumber - difference)) || (col > (mediumNumber + difference))) {
                    System.out.print('-');
                } else {
                    System.out.print(count++);
                }
            }
            System.out.println();
        }
    }
}
