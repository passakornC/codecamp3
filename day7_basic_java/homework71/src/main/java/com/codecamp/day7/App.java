package com.codecamp.day7;

public class App {
    public static void main(String[] args) {
        App m = new App();

        for (int number = 2; number <= 4; number++) {
            System.out.println("Number: " + number);

            m.draw4(number);

            System.out.println();
            System.out.println();
        }
    }

    private void draw1(int number) {
        for (int col = 0; col < number; col++) {
            System.out.print('*');
        }
    }

    private void draw2(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    private void draw3(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print(col + 1);
            }
            System.out.println();
        }
    }

    private void draw4(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print(number - col);
            }
            System.out.println();
        }
    }

    private void draw5(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print(row + 1);
            }
            System.out.println();
        }
    }

    private void draw6(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print(number - row);
            }
            System.out.println();
        }
    }

    private void draw7(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print((row * number) + (col + 1));
            }
            System.out.println();
        }
    }

    private void draw8(int number) {
        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                System.out.print((number * number) - (row * number + col));
            }
            System.out.println();
        }
    }
}

