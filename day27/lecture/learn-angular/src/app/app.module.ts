import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HomeComponent} from "./home/home.component";
import { NavBarComponent } from './home/nav-bar/nav-bar.component';
import { TopBannerComponent } from './home/top-banner/top-banner.component';
import { CategoryMenuComponent } from './home/category-menu/category-menu.component';
import { SearchBarComponent } from './home/nav-bar/search-bar/search-bar.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { Lab4ComponentComponent } from './lab4-component/lab4-component.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavBarComponent,
    TopBannerComponent,
    CategoryMenuComponent,
    SearchBarComponent,
    ToDoListComponent,
    Lab4ComponentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
