import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {
  private taskList: string[];

  constructor() {
    this.taskList = [];
  }

  ngOnInit() {
  }

  addTask(fromInput: string) {
    this.taskList.push(fromInput);
  }

}
