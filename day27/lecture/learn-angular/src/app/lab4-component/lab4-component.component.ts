import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lab4-component',
  templateUrl: './lab4-component.component.html',
  styleUrls: ['./lab4-component.component.css']
})
export class Lab4ComponentComponent implements OnInit {
  private numberArray: number[];

  private isTrue;

  constructor() {
    this.numberArray = [];

    this.isTrue = true;
  }

  ngOnInit() {
    for(let i: number = 1; i <= 10; i++)
      this.numberArray.push(i);
  }

}
