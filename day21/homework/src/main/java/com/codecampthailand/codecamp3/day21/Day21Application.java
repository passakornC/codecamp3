package com.codecampthailand.codecamp3.day21;

import com.codecampthailand.codecamp3.day21.config.AppConfig;
import com.codecampthailand.codecamp3.day21.domain.BankAccount;
import com.codecampthailand.codecamp3.day21.domain.Person;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Day21Application {

    public static void main(String[] args) {
        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {

            /**
             * Person
             */
            Person person01 = context.getBean(Person.class);
            person01.setFirstName("F01");
            person01.setLastName("L01");
            person01.setId("1000");

//            System.out.printf("%s %s %s\n", person01.getId(), person01.getFirstName(), person01.getLastName());

            /**
             * Bank Account
             */
            BankAccount account01 = context.getBean(BankAccount.class);

            // open a new account
//            account01.open("1004", person01, 5000.00);

            // withdraw
//            account01.withdraw("1002", 3000);

            // deposit
            account01.deposit("1002", 3000);

            // close
//            account01.close("1000");
        }
    }

}
