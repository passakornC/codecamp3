package com.codecampthailand.codecamp3.day21.config;

import com.codecampthailand.codecamp3.day21.domain.Car;
import com.codecampthailand.codecamp3.day21.domain.Engine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan({"com.codecampthailand.codecamp3.day21.service", "com.codecampthailand.codecamp3.day21.servlet"})
public class Hw2Config {

//    @Bean
//    public CarService carService() {
//        return new CarService();
//    }

//    @Bean
//    public CarServlet CarServlet() {
//        return new CarServlet(Arrays.asList(new LowModel(), new MiddleModel(), new HighModel(), new SuperModel()));
//    }

    @Bean
    public List<List<Car>> carList() {
        Engine engine;
        Car car;

        List<Car> lowModels = new ArrayList<>();
        engine = new Engine();
        engine.setHorsePower(80);
        engine.setModel("1300 cc V4");
        car = new Car();
        car.setName("Toyota Yaris");
        car.setEngine(engine);
        lowModels.add(car);

        List<Car> middleModels = new ArrayList<>();
        car = new Car();
        car.setName("Honda Civic");
        engine = new Engine();
        engine.setHorsePower(150);
        engine.setModel("1800 cc V4");
        car.setEngine(engine);
        middleModels.add(car);

        List<Car> highModels = new ArrayList<>();
        car = new Car();
        car.setName("BMW 318i");
        engine = new Engine();
        engine.setHorsePower(200);
        engine.setModel("1800 cc V6");
        car.setEngine(engine);
        highModels.add(car);

        List<Car> superModels = new ArrayList<>();
        car = new Car();
        car.setName("Lotus Esprit");
        engine = new Engine();
        engine.setHorsePower(500);
        engine.setModel("5000 cc V12");
        car.setEngine(engine);
        superModels.add(car);

        return Arrays.asList(lowModels, middleModels, highModels, superModels);
    }
}
