package com.codecampthailand.codecamp3.day21.domain;

import org.springframework.stereotype.Component;

@Component("Person")
public class Person {
    /********************
     * Property
     ********************/
    private String id;
    private String firstName;
    private String lastName;


    /********************
     * Constructor
     ********************/
    public Person() {}

    public Person(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }


    /********************
     * Method
     ********************/

    /********************
     * Getter
     ********************/
    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    /********************
     * Setter
     ********************/
    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
