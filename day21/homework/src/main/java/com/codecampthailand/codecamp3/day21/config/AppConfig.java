package com.codecampthailand.codecamp3.day21.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.codecampthailand.codecamp3.day21.domain")
public class AppConfig {
}
