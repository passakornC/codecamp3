package com.codecampthailand.codecamp3.day21.utility;

import com.codecampthailand.codecamp3.day21.domain.BankAccount;
import com.mysql.cj.jdbc.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

public class DbWorker {
    /********************
     * Property
     ********************/
    private static DbWorker dbWorker = null;
    private Connection connection;


    /********************
     * Constructor
     ********************/
    public DbWorker(String host, String usr, String pwd, String db) {
        try {
            DriverManager.registerDriver(new Driver());
            connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + db, usr, pwd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /********************
     * Method
     ********************/
    public static DbWorker getDbWorker() {
        if (Objects.isNull(dbWorker)) {
            dbWorker = new DbWorker("localhost:3307", "root", "1234", "day21");
        }

        return dbWorker;
    }

    public ArrayList<BankAccount> bankAccountsQueryGet(String sql, String[] bindValue) {
        ArrayList<BankAccount> accountArrayList = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);

            for (int i = 0; i < bindValue.length; i++) {
                ps.setString(i + 1, bindValue[i]);
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BankAccount ba = new BankAccount(rs);
                accountArrayList.add(ba);
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accountArrayList;
    }

    public int insertData(String sql, String[] bindValue) {
        int returnValue = -1;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);

            for (int i = 0; i < bindValue.length; i++) {
                ps.setString(i + 1, bindValue[i]);
            }

            returnValue = ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    public int updateData(String sql, String[] bindValue) {
        return insertData(sql, bindValue);
    }


    /********************
     * Getter
     ********************/
    public Connection getConnection() {
        return connection;
    }
}
