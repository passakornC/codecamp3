package com.codecampthailand.codecamp3.day21;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class TestRun {
    public static void main(String[] args) throws LifecycleException {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir("temp");
        tomcat.setPort(8090);
        tomcat.addWebapp("/", new File("src/main/webapp").getAbsolutePath());
        tomcat.getConnector();
        tomcat.start();
    }
}
