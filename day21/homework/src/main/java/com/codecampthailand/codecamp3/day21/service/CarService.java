package com.codecampthailand.codecamp3.day21.service;

import com.codecampthailand.codecamp3.day21.domain.Car;
import com.codecampthailand.codecamp3.day21.domain.Engine;
import com.codecampthailand.codecamp3.day21.domain.PriceRange;

import java.util.ArrayList;
import java.util.List;

//@Component
public class CarService {


    public List<Car> getCars(PriceRange priceRange) {
        switch (priceRange) {
            case LOW : return getLowModels();
            case MIDDLE : return getMiddleModels();
            case HIGH : return getHighModels();
            case SUPER : return getSuperModels();
            default : break;
        }
        return new ArrayList<Car>();
    }

    private List<Car> getLowModels() {
        List<Car> lowModels = new ArrayList<>();

        Engine engine = new Engine();
        engine.setHorsePower(80);
        engine.setModel("1300 cc V4");

        Car car = new Car();
        car.setName("Toyota Yaris");
        car.setEngine(engine);

        lowModels.add(car);

        return lowModels;
    }

    private List<Car> getMiddleModels() {
        List<Car> middleModels = new ArrayList<>();
        Car car = new Car();
        car.setName("Honda Civic");
        Engine engine = new Engine();
        engine.setHorsePower(150);
        engine.setModel("1800 cc V4");
        car.setEngine(engine);
        middleModels.add(car);
        return middleModels;
    }

    private List<Car> getHighModels() {
        List<Car> highModels = new ArrayList<>();
        Car car = new Car();
        car.setName("BMW 318i");
        Engine engine = new Engine();
        engine.setHorsePower(200);
        engine.setModel("1800 cc V6");
        car.setEngine(engine);
        highModels.add(car);
        return highModels;
    }

    private List<Car> getSuperModels() {
        List<Car> superModels = new ArrayList<>();
        Car car = new Car();
        car.setName("Lotus Esprit");
        Engine engine = new Engine();
        engine.setHorsePower(500);
        engine.setModel("5000 cc V12");
        car.setEngine(engine);
        superModels.add(car);
        return superModels;
    }

}