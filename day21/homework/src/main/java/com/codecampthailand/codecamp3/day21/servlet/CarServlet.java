package com.codecampthailand.codecamp3.day21.servlet;

import com.codecampthailand.codecamp3.day21.config.Hw2Config;
import com.codecampthailand.codecamp3.day21.domain.Car;
import com.codecampthailand.codecamp3.day21.domain.PriceRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CarServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Autowired
    private List<List<Car>> carList;

    public CarServlet() {}

	public List<List<Car>> getCarList() {
		return carList;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String priceRange = request.getParameter("priceRange");

		try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Hw2Config.class)) {
			CarServlet carServlet = context.getBean(CarServlet.class);
			carList = carServlet.getCarList();

			List<Car> cars = new ArrayList<>();
			switch (PriceRange.valueOf(priceRange)) {
				case LOW : cars = carList.get(0); break;
				case MIDDLE : cars = carList.get(1); break;
				case HIGH : cars = carList.get(2); break;
				case SUPER : cars = carList.get(3); break;
				default : break;
			}

			try {
				request.setAttribute("cars", cars);
				response.setHeader("status", "success");
				RequestDispatcher dispatcher = request.getRequestDispatcher("result.jsp");
				dispatcher.forward(request, response);
			} catch (Exception e) {
				response.sendRedirect("index.jsp");
			}
		}

	}

}