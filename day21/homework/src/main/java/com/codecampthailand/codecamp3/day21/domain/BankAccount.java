package com.codecampthailand.codecamp3.day21.domain;

import com.codecampthailand.codecamp3.day21.utility.DbWorker;
import com.mysql.cj.jdbc.Driver;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;


/********************
 * Enum
 ********************/
enum AccountStatus {
    Inactive,
    Active;
}


@Component("BankAccount")
public class BankAccount {
    /********************
     * Property
     ********************/
    private String accountNo;
    private BigDecimal amount;
    private int active;
    private Person owner;


    /********************
     * Constructor
     ********************/
    public BankAccount() {}

    public BankAccount(ResultSet resultSet) {
        try {
            this.accountNo = resultSet.getString("account_no");
            this.active = resultSet.getInt("active");
            this.amount = resultSet.getBigDecimal("amount");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    public BankAccount(BigDecimal amount, Person owner) {
//        this.amount = amount;
//        this.owner = owner;
//    }


    /********************
     * Method
     ********************/
    public void open(String accountNo, Person owner, double amount) {
        int success = -1;

        if (amount >= 0) {
            DbWorker dbWorker = DbWorker.getDbWorker();
            String sql = "insert into bank_account (account_no, active, amount) value (?, ?, ?)";
            String[] bindValue = new String[] {accountNo, String.valueOf(AccountStatus.Active.ordinal()), BigDecimal.valueOf(amount).toPlainString()};
            success = dbWorker.insertData(sql, bindValue);
        } else {
            System.out.println("The amount must greater than zero.");
        }

        if (success == 1) {
            System.out.printf("Account no: %s [%s %s] is opened. The current balance is %.2f\n", accountNo, owner.getFirstName(), owner.getLastName(), amount);
        } else {
            System.out.println("Sorry, opening an account cannot be accomplished.");
        }
    }

    public void close(String accountNo) {
        DbWorker dbWorker = DbWorker.getDbWorker();
        String sql = "update bank_account set active = " + String.valueOf(AccountStatus.Inactive.ordinal()) + " where account_no = ?";
        String[] bindValue = new String[] {accountNo};
        int success = dbWorker.updateData(sql, bindValue);

        if (success == 1) {
            System.out.printf("Account no: %s was closed.\n", accountNo);
        } else {
            System.out.println("Sorry, this account cannot be closed.");
        }
    }

    public void withdraw(String accountNo, double amount) {
        int success = -1;

        if (amount >= 0) {
            if (getCurrentBalance(accountNo).compareTo(BigDecimal.valueOf(amount)) >= 0) {
                DbWorker dbWorker = DbWorker.getDbWorker();
                String sql = "update bank_account set amount = amount - ? where account_no = ?";
                String[] bindValue = new String[] {String.valueOf(BigDecimal.valueOf(amount)), accountNo};
                success = dbWorker.updateData(sql, bindValue);
            } else {
                System.out.println("Sorry, the money is not enough.");
            }
        } else {
            System.out.println("The amount must greater than zero.");
        }

        if (success == 1) {
            System.out.printf("Withdraw successful. The current balance is %.2f\n", getCurrentBalance(accountNo));
        } else {
            System.out.println("Sorry, this transaction cannot be completed.");
        }
    }

    public void deposit(String accountNo, double amount) {
        int success = -1;

        if (amount >= 0) {
            DbWorker dbWorker = DbWorker.getDbWorker();
            String sql = "update bank_account set amount = amount + ? where account_no = ?";
            String[] bindValue = new String[] {String.valueOf(BigDecimal.valueOf(amount)), accountNo};
            success = dbWorker.updateData(sql, bindValue);
        } else {
            System.out.println("The amount must greater than zero.");
        }

        if (success == 1) {
            System.out.printf("Deposit successful. The current balance is %.2f\n", getCurrentBalance(accountNo));
        } else {
            System.out.println("Sorry, this transaction cannot be completed.");
        }
    }

    private BigDecimal getCurrentBalance(String accountNo) {
        DbWorker dbWorker = DbWorker.getDbWorker();
        String sql = "select * from bank_account where account_no = ?";
        String[] bindValue = new String[] {accountNo};
        return dbWorker.bankAccountsQueryGet(sql, bindValue).get(0).getAmount();
    }


    /********************
     * Getter
     ********************/
    public String getAccountNo() {
        return accountNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Person getOwner() {
        return owner;
    }


    /********************
     * Setter
     ********************/
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
