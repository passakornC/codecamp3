package com.codecampthailand.codecamp3.day21.domain;

public class Engine {

    private String model;

    private double horsePower;

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getHorsePower() {
        return this.horsePower;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }

}