package db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name= "bank_account")
public class BankAccount {

    @Id
    @Column(name = "account_no", unique = true)
    private String accountNo;
    @Column(name = "balance")
    private BigDecimal balance;
    @Column(name = "status")
    private Integer status;

    public BankAccount() {
    }

    public BankAccount(String accountNo, BigDecimal balance, Integer status) {
        this.accountNo = accountNo;
        this.balance = balance;
        this.status = status;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public BigDecimal getBalance() {
        return balance;
    }
    public Integer getStatus() {
        return status;
    }
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "[ACC_NO=" + accountNo + ", BALANCE=" + balance + ", STATUS=" + status + "]";
    }

}



