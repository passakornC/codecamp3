package db;

import db.BankAccount;
import db.BankDao;
import db.BankService;
import db.Person;

import java.math.BigDecimal;

public class BankServiceImpl implements BankService {
    @Override
    public void openAccount(Person person) {
        BankDao.openAccount(person);
    }

    @Override
    public void closeAccount(BankAccount ba) {
        ba.setStatus(0);
        BankDao.updateStatusBankAccount(ba);
    }

    @Override
    public void withdraw(BankAccount ba, BigDecimal amount) {
        BigDecimal balance = BankDao.findBankAccountByNo(ba).get().getBalance();
        if (balance.compareTo(amount) >= 0) {
            ba.setBalance(balance.subtract(amount));
            BankDao.updateBalanceBankAccount(ba);
        }
    }

    @Override
    public void deposit(BankAccount ba, BigDecimal amount) {
        BigDecimal balance = BankDao.findBankAccountByNo(ba).get().getBalance();
        ba.setBalance(balance.add(amount));
        BankDao.updateBalanceBankAccount(ba);
    }
}
