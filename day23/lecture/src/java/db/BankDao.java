package db;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Optional;

public class BankDao {
    public final static Logger logger = LogManager.getLogger(BankDao.class);

    public static String openAccount(Person person) {
        // Homework2
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DbConfig.class)) {
            PersonRepository personRepository = context.getBean(PersonRepository.class);
            personRepository.save(person);
        }

//        Homework1
//        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
//        sessionObj.save(person);
//        Transaction transObj = sessionObj.beginTransaction();
//        transObj.commit();
//        sessionObj.close();

        logger.info("Successfully Created " + person.toString());
        return person.getBankAccount().getAccountNo();
    }

    @SuppressWarnings("unchecked")
    public static List<Person> getAllPeople() {
        // Homework2


//        // Homework1
        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
        List<Person> people = sessionObj.createQuery("FROM Person").list();
        sessionObj.close();

        logger.info("Person Records Available In Database Are?= " + people.size());
        return people;
    }

    public static void updateStatusBankAccount(BankAccount bankAccount) {
        // Homework2
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DbConfig.class)) {
            BankAccountRepository bankAccountRepository = context.getBean(BankAccountRepository.class);
            Optional<BankAccount> bankAccountOptional = bankAccountRepository.findById(bankAccount.getAccountNo());
            if (bankAccountOptional.isPresent()) {
                bankAccountOptional.get().setStatus(bankAccount.getStatus());
                bankAccountRepository.save(bankAccountOptional.get());
                logger.info("Bank Account Record Is Successfully Updated!= " + bankAccountOptional.get().toString());
            }
        }
    }

    public static void updateBalanceBankAccount(BankAccount bankAccount) {
        // Homework2
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DbConfig.class)) {
            BankAccountRepository bankAccountRepository = context.getBean(BankAccountRepository.class);
            Optional<BankAccount> bankAccountOptional = bankAccountRepository.findById(bankAccount.getAccountNo());
            if (bankAccountOptional.isPresent()) {
                bankAccountOptional.get().setBalance(bankAccount.getBalance());
                bankAccountRepository.save(bankAccountOptional.get());
                logger.info("Bank Account Record Is Successfully Updated!= " + bankAccountOptional.get().toString());
            }
        }



        // Homework1
//        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
//        Transaction transObj = sessionObj.beginTransaction();
//        BankAccount bankAccountObj = (BankAccount) sessionObj.load(BankAccount.class, bankAccount.getAccountNo());
//        bankAccountObj.setBalance(bankAccount.getBalance());
//        bankAccountObj.setStatus(bankAccount.getStatus());
//        sessionObj.update(bankAccountObj);
//        transObj.commit();
//        sessionObj.close();
//        logger.info("Bank Account Record Is Successfully Updated!= " + bankAccountObj.toString());
    }

    public static void updatePerson(Person person) {
        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
        Transaction transObj = sessionObj.beginTransaction();
        Person personObj = (Person) sessionObj.load(Person.class, person.getPersonId());
        personObj.setStatus(person.getStatus());
        personObj.setFullName(person.getFullName());
        sessionObj.update(personObj);
        transObj.commit();
        sessionObj.close();
        logger.info("Person Is Successfully Updated!= " + personObj.toString());
    }

    public static Person findPersonById(Person person) {
        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
        Person personObj = (Person) sessionObj.load(Person.class, new PersonId(person.getPersonId(), person.getBankAccount().getAccountNo()));
        sessionObj.close();
        return personObj;
    }

    public static void deletePerson(Person person) {
        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
        Transaction transObj = sessionObj.beginTransaction();
        Person personObj = findPersonById(person);
        sessionObj.delete(personObj);
        transObj.commit();
        sessionObj.close();
        logger.info("Successfully Record Is Successfully Deleted!=  " + person.toString());
    }

    public static Optional<BankAccount> findBankAccountByNo(BankAccount bankAccount) {
        Optional<BankAccount> bankAccountOptional = Optional.empty();

        // Homework2
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DbConfig.class)) {
            BankAccountRepository bankAccountRepository = context.getBean(BankAccountRepository.class);
            bankAccountOptional = bankAccountRepository.findById(bankAccount.getAccountNo());
        }

        return bankAccountOptional;

        // Homework1
//        Session sessionObj = HibernateUtil.getSessionFactory().openSession();
//        BankAccount bankAccountObj = (BankAccount) sessionObj.load(BankAccount.class, bankAccount.getAccountNo());
//        bankAccount = new BankAccount(bankAccountObj.getAccountNo(), bankAccountObj.getBalance(), bankAccountObj.getStatus()); // solution for lazy loading
//        sessionObj.close();
//
//        return bankAccount;
    }


}

