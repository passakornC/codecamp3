package db;

import db.BankAccount;
import db.BankDao;
import db.Person;

import java.math.BigDecimal;

public interface BankService {
    public void openAccount(Person person);
    public void closeAccount(BankAccount bankAccount);
    public void withdraw(BankAccount bankAccount, BigDecimal amount);
    public void deposit(BankAccount bankAccount, BigDecimal amount);
}
