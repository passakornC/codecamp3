package db;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.math.BigDecimal;
import java.util.List;

public class TestDB {
    public static void main(String[] args) {
//        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DbConfig.class)) {
//            PersonRepository personRepository = context.getBean(PersonRepository.class);

            BankAccount ba0 = new BankAccount("ACC_0", BigDecimal.valueOf(1000.00), 1);
            BankAccount ba1 = new BankAccount("ACC_1", BigDecimal.valueOf(100.00), 1);
            BankAccount ba2 = new BankAccount("ACC_2", BigDecimal.valueOf(200.00), 1);
            BankAccount ba3 = new BankAccount("ACC_3", BigDecimal.valueOf(300.00), 1);

            Person p0 = new Person("CUST0", ba0, "TestPerson0", 1);
            Person p1 = new Person("CUST1", ba1, "TestPerson1", 1);
            Person p2 = new Person("CUST2", ba2, "TestPerson2", 1);
            Person p3 = new Person("CUST2", ba3, "TestPerson2", 1);


//            personRepository.save(p1);
//            personRepository.save(p2);


            BankService bankService = new BankServiceImpl();
            /**
             * Open an account
             */
//            bankService.openAccount(p0);
//            bankService.openAccount(p2);
//            bankService.openAccount(p3);

            /**
             * Close an account
             */
//            bankService.closeAccount(ba3);

            /**
             * Deposit
             */
//            bankService.deposit(ba1, BigDecimal.valueOf(2000.00));

            /**
             * Withdraw
             */
//            bankService.withdraw(ba1, BigDecimal.valueOf(151));



            //            personRepository.save(p1);
//            personRepository.save(p2_1);
//            personRepository.save(p2_2);

//            BankAccountRepository bankAccountRepository = context.getBean(BankAccountRepository.class);
//            ba1.setBalance(BigDecimal.valueOf(99.00));
//            bankAccountRepository.save(ba1);
//
//            List<Person> personList = personRepository.getByPersonId("CUST2");
//            for (Person person: personList) {
//                System.out.println(person);
//            }
//
//            personRepository.delete(p1);
//            personList = (List<Person>) personRepository.findAll();
//            for (Person person: personList) {
//                System.out.println(person);
//            }

//            BankService bankService = new BankServiceImpl();
            //        bankService.openAccount(p1);
            //        bankService.openAccount(p2_1);
            //        bankService.openAccount(p2_2);

            //        bankService.deposit(ba1, BigDecimal.valueOf(99.00));
            //        bankService.withdraw(ba1, BigDecimal.valueOf(99.00));
//            bankService.closeAccount(ba1);
        }
    }
//}