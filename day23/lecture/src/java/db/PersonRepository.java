package db;

import db.Person;
import db.PersonId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CrudReposity<ClassName, TypeOfPrimaryKey>
 */


@Repository
public interface PersonRepository extends CrudRepository<Person, PersonId> {
    public List<Person> getByPersonId(String personId);
}
