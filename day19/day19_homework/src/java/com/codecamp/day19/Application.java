package com.codecamp.day19;

import java.io.File;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

public class Application {
    public static void main(String[] args) throws LifecycleException {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir("temp");
        tomcat.setPort(8080);

        Application application = new Application();

        // url start from http://localhost:8080/
        tomcat.addWebapp("", new File("src/main/webapp").getAbsolutePath());
        tomcat.getConnector();
        tomcat.start();
    }
}
