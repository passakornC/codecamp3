package com.codecamp.day19.model;

import com.codecamp.day19.MyDb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Course {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTeachBy() {
        return teachBy;
    }

    public void setTeachBy(String teachBy) {
        this.teachBy = teachBy;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public MyDb getDbConnection() {
        return dbConnection;
    }

    public void setDbConnection(MyDb dbConnection) {
        this.dbConnection = dbConnection;
    }

    private String id;
    private String name;
    private String detail;
    private String price;
    private String teachBy;
    private String created_at;

    private MyDb dbConnection;

    public Course(MyDb dbConnection) {
        this.dbConnection = dbConnection; // Constructor Injection
    }

    public void SetDbConnection(MyDb newConnection) {
        this.dbConnection = newConnection; // Property Injection
    }

    public ArrayList<Course> findById(String id) {
        String sql = "SELECT * FROM courses WHERE id = ?";
        String[] bindValues = new String[]{id};
        return dbConnection.courseQueryGet(sql, bindValues);
    }

    public ArrayList<Course> findByPrice(String price) {
        String sql = "SELECT * FROM courses WHERE price = ?";
        String[] bindValues = new String[]{price};
        return dbConnection.courseQueryGet(sql, bindValues);
    }


    public Course(ResultSet resultSet) {
        try {
            this.id = resultSet.getString("id");
            this.name = resultSet.getString("name");
            this.detail = resultSet.getString("detail");
            this.price = resultSet.getString("price");
            this.teachBy = resultSet.getString("teach_by");
            this.created_at = resultSet.getString("created_at");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
