package com.codecamp.day19.model;

import com.codecamp.day19.MyDb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Instructor {
    private String id;
    private String name;
    private String created_at;

    private MyDb dbConnection;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public MyDb getDbConnection() {
        return dbConnection;
    }

    public void setDbConnection(MyDb dbConnection) { // property injection
        this.dbConnection = dbConnection;
    }

    public Instructor() {}

    public Instructor(MyDb dbConnection) {
        this.dbConnection = dbConnection; // Constructor Injection
    }

    public ArrayList<Instructor> findAll() {
        String sql = "SELECT * FROM instructors";
        String[] bindValues = new String[]{};
        return dbConnection.instructorQueryGet(sql, bindValues);
    }

    public ArrayList<Instructor> findById(String id) {
        String sql = "SELECT * FROM instructors WHERE id = ?";
        String[] bindValues = new String[]{id};
        return dbConnection.instructorQueryGet(sql, bindValues);
    }

    public Instructor(ResultSet resultSet) {
        try {
            this.id = resultSet.getString("id");
            this.name = resultSet.getString("name");
            this.created_at = resultSet.getString("created_at");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
