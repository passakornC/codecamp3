package com.codecamp.day19;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.jdbc.Driver;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class InstructorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        final String FIND_ALL_REGEX = ".*find_all.*";
        String reqUrl = req.getServletPath();
        String idFromUser = req.getParameter("id");
        String dbUrl = "jdbc:mysql://localhost:3306/day17";
        String dbUsr = "root";
        String dbPwd = "1234";
        String sql = "SELECT * FROM instructors";
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        // find_by_id
        if (!reqUrl.matches(FIND_ALL_REGEX)) {
            sql += " WHERE id = ?";
        }

        // read from db
        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection(dbUrl, dbUsr, dbPwd);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            if (!reqUrl.matches(FIND_ALL_REGEX)) {
                preparedStatement.setString(1, idFromUser);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                String created_at = resultSet.getString("created_at");

                // create an object
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", id);
                hashMap.put("name", name);
                hashMap.put("created_at", created_at);
                arrayList.add(hashMap);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        // create json
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonOutput = "";


        try {
            jsonOutput = objectMapper.writeValueAsString(arrayList);
        } catch (JsonProcessingException ex) {
            System.out.println("There's something wrong with writeValueAsString: " +
                    ex.getMessage());
        }

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print(jsonOutput);
        outputStream.flush();
        outputStream.close();
    }
}
