package com.codecamp.day19;

import com.codecamp.day19.model.Course;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.jdbc.Driver;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class CourseHw2Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        final String FIND_BY_ID_REGEX = ".*find_by_id.*";
        String reqUrl = req.getServletPath();
        String idFromUser = req.getParameter("id");
        String priceFromUser = req.getParameter("price");
        ArrayList<Course> arrayList = new ArrayList<>();

        if (reqUrl.matches(FIND_BY_ID_REGEX)) { // find_by_id
            arrayList = this.getCourseById(idFromUser);
        } else { // find_by_price
            arrayList = this.getCourseByPrice(priceFromUser);
        }

        // create json
        this.printOutputStream(resp, arrayList);
    }

    protected void printOutputStream(HttpServletResponse resp, ArrayList<Course> arrayList) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonOutput = "";

        try {
            jsonOutput = objectMapper.writeValueAsString(arrayList);
        } catch (JsonProcessingException ex) {
            System.out.println("There's something wrong with writeValueAsString: " +
                    ex.getMessage());
        }

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print(jsonOutput);
        outputStream.flush();
        outputStream.close();
    }

    protected ArrayList<Course> getCourseById(String id) {
        MyDb myDb = MyDb.getInstance();
        Course course = new Course(myDb);
        return course.findById(id);
    }

    protected ArrayList<Course> getCourseByPrice(String price) {
        MyDb myDb = MyDb.getInstance();
        Course course = new Course(myDb);
        return course.findByPrice(price);
    }
}
