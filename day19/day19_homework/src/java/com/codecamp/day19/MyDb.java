package com.codecamp.day19;

import com.codecamp.day19.model.Course;
import com.codecamp.day19.model.Instructor;
import com.mysql.cj.jdbc.Driver;

import java.sql.*;
import java.util.ArrayList;

public class MyDb {
    private static MyDb instance = null;
    private Connection connection;

    public MyDb(String host, String username, String password, String database) {
        try {
            DriverManager.registerDriver(new Driver());
            connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static MyDb getInstance() {
        if (instance == null) {
            instance = new MyDb("localhost:3306", "root", "1234", "day17");
        }

        return instance;
    }

    public ArrayList<Course> courseQueryGet(String sql, String[] bindValues) {
        ArrayList<Course> arrayList = new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement(sql);

            for (int i=0; i < bindValues.length; i++) {
                statement.setString(i+1, bindValues[i]);
            }

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Course course = new Course(resultSet);
                arrayList.add(course);
            }
            statement.close();
        } catch (SQLException ex) {
            System.out.println("Query Error : "+sql);
            System.out.println("Exception: "+ex.toString());
        }

        return arrayList;
    }

    public ArrayList<Instructor> instructorQueryGet(String sql, String[] bindValues) {
        ArrayList<Instructor> arrayList = new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement(sql);

            for (int i=0; i < bindValues.length; i++) {
                statement.setString(i+1, bindValues[i]);
            }

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Instructor instructor = new Instructor(resultSet);
                arrayList.add(instructor);
            }
            statement.close();
        } catch (SQLException ex) {
            System.out.println("Query Error : "+sql);
            System.out.println("Exception: "+ex.toString());
        }

        return arrayList;
    }

}
