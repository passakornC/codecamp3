package com.codecamp.day19;

import com.codecamp.day19.model.Course;
import com.codecamp.day19.model.Instructor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class InstructorHw2Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        final String FIND_ALL_REGEX = ".*find_all.*";
        String reqUrl = req.getServletPath();
        String idFromUser = req.getParameter("id");
        ArrayList<Instructor> arrayList = new ArrayList<>();

        // find_all
        arrayList = this.getAllInstructors();
        // find_by_id
        if (!reqUrl.matches(FIND_ALL_REGEX)) {
            arrayList = this.getInstructorById(idFromUser);
        }

        // create json
        this.printOutputStream(resp, arrayList);
    }

    protected void printOutputStream(HttpServletResponse resp, ArrayList<Instructor> arrayList) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonOutput = "";

        try {
            jsonOutput = objectMapper.writeValueAsString(arrayList);
        } catch (JsonProcessingException ex) {
            System.out.println("There's something wrong with writeValueAsString: " +
                    ex.getMessage());
        }

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print(jsonOutput);
        outputStream.flush();
        outputStream.close();
    }

    protected ArrayList<Instructor> getAllInstructors() {
        MyDb myDb = MyDb.getInstance();
        Instructor instructor = new Instructor();
        instructor.setDbConnection(myDb);
        return instructor.findAll();
    }

    protected ArrayList<Instructor> getInstructorById(String id) {
        MyDb myDb = MyDb.getInstance();
        Instructor instructor = new Instructor(myDb);
        return instructor.findById(id);
    }
}
