package com.codecamp.day9;

public class Homework9_1 {
    public static void main( String[] args ) {
        CEO ceo = new CEO("Captain", "bLastName", 500_000);
        Employee employee = new Employee("Somchai", "aLastName", 200_000);

        System.out.println(employee.sayHelloTo(ceo.getFirstname()));
        System.out.println(ceo.sayHelloTo(employee.getFirstname()));

        System.out.println(ceo.fire(employee));
        System.out.println(ceo.hire(employee));

        System.out.println(ceo.seminar());
        System.out.println(ceo.golf());

        System.out.println(ceo.assignNewSalary(employee, 1_000));
        System.out.println(ceo.assignNewSalary(employee, 250_000));
    }
}
