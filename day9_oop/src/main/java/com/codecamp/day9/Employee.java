package com.codecamp.day9;

public class Employee {
    private int salary;
    private String firstname;
    private String lastname;

    public Employee (String firstname, String lastname, int salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public String sayHelloTo(String name) {
        return "Hey " + name + ", Today is very cold!";
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void gossip() {}

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary () {
        return this.salary;
    }
}
