package com.codecamp.day9;

public class CEO extends Employee {
    private String dressCode;

    public CEO(String firstname, String lastname, int salary) {
        super(firstname, lastname, salary);
        this.dressCode = "suit";
    }

    public String assignNewSalary(Employee employee, int newSalary) {
        if (newSalary > employee.getSalary()) {
            this.setSalary(newSalary);
            return employee.getFirstname() + "\'s salary has been set to " + this.getSalary();
        } else {
            return employee.getFirstname() + "\'s salary is less than before!!";
        }
    }

    public String fire(Employee employee) {
        this.dressCode = "tshirt";
        return employee.getFirstname() + " has been fired! Dress with :" + this.dressCode;
    }

    public String hire(Employee employee) {
        this.dressCode = "tshirt";
        return employee.getFirstname() + " has been hired back! Dress with :" + this.dressCode;
    }

    public String seminar() {
        this.dressCode = "suit";
        return "He is going to seminar. Dress with :" + this.dressCode;
    }

    public String golf () {
        this.dressCode = "golf_dress";
        return "He goes to golf club to find a new connection. Dress with :" + this.dressCode;
    }
}
