package com.codecamp.day17;

import com.mysql.cj.jdbc.Driver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class TotalCostServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String databaseURL = "jdbc:mysql://localhost:3306/day16";
        final String databaseUser = "root";
        final String databasePassword = "1234";
        String sqlCommand = "select s.name, sum(c.price) as total " +
                "from students s " +
                "left join enrolls e on s.id = e.student_id " +
                "left join courses c on e.course_id = c.id " +
                "group by s.id";

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlCommand);

            ArrayList<HashMap<String, String>> totalCostList = new ArrayList<>();
            while (resultSet.next()) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("student_name", resultSet.getString("s.name"));
                hashMap.put("total", ((Integer) resultSet.getInt("total")).toString());

                totalCostList.add(hashMap);
            }
            req.setAttribute("totalCostList", totalCostList);
            req.getRequestDispatcher("jsp/total-cost.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
