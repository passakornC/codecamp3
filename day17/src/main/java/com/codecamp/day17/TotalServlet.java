package com.codecamp.day17;

import com.mysql.cj.jdbc.Driver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class TotalServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String databaseURL = "jdbc:mysql://localhost:3306/day16";
        final String databaseUser = "root";
        final String databasePassword = "1234";
        String sqlCommand = "select sum(c.price) as total " +
                "from enrolls e " +
                "left join courses c on e.course_id = c.id";

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlCommand);
            resultSet.next();
            int total = resultSet.getInt("total");
            req.setAttribute("total", total);
            req.getRequestDispatcher("jsp/total.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
