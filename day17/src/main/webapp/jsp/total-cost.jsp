<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-28
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>Student name</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${totalCostList}" var="item">
            <tr>
                <td><c:out value="${item.student_name}" /></td>
                <td><c:out value="${item.total}" /></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
</body>
</html>
