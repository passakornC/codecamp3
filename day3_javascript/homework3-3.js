let url = "./homework1-4.json";

fetch(url)
    .then(resp => resp.json())
    .then(data => {
        // console.log(data);
        let eyeColorMap = new Map();
        let genderMap = new Map();
        let friendList = [];

        for (let index in data) {
            countValue(data[index], eyeColorMap, 'eyeColor');
            countValue(data[index], genderMap, 'gender');
            countFriend(data[index], friendList);
        }

        $('#eye-color-table').append(printTable(eyeColorMap));
        $('#gender-table').append(printTable(genderMap));
        $('#personTable').append(printPersonTable(friendList));
    });

/* <---------- functions ----------> */
function printTable(map) {
    let tableTag = '<table>';
    for (let dataOneRow of map) {
        tableTag += `<tr><td class="text-align-right">${dataOneRow[0]}</td><td class="text-align-left">${dataOneRow[1]}</td></tr>`;
    }
    tableTag += '</table>';

    return tableTag;
}

function printPersonTable(list) {
    let tableTag = '<table style="text-align:center;">';
    // header
    tableTag += '<thead style="background-color:black;color:white;"><tr>';
    for (let key in list[0]) {
        tableTag += `<th>${key}</th>`;
    }
    tableTag += '</tr></thead>';

    // body
    tableTag += '<tbody>';
    for (let index in list) {
        tableTag += `<tr><td><img src="${list[index]['Photo']}" /></td>`;
        tableTag += `<td>${list[index]['Name']}</td>`;
        tableTag += `<td>${list[index]['Friend Count']}</td>`;
        tableTag += `<td>${list[index]['Email']}</td>`;
        tableTag += `<td>${list[index]['Company']}</td>`;
        tableTag += `<td>${list[index]['Balance']}</td></tr>`;
    }
    tableTag += '</tbody></table>';

    return tableTag;
}

function countValue(data, map, key) {
    if (map.has(data[key])) {
        map.set(data[key], map.get(data[key]) + 1);
    } else {
        map.set(data[key], 1);
    }
}

function countFriend(data, list) {
    let obj = {};

    obj['Photo'] = data['picture'];
    obj['Name'] = data['name'];
    obj['Friend Count'] = data['friends'].length;
    obj['Email'] = data['email'];
    obj['Company'] = data['company'];
    obj['Balance'] = data['balance'];

    list.push(obj);
}
