const peopleSalary = [
    {
        "id": "1001",
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": 40000
    },
    {
        "id": "1002",
        "firstname": "Tony",
        "lastname": "Stark",
        "company": "Marvel",
        "salary": 1000000
    },
    {
        "id": "1003",
        "firstname": "Somchai",
        "lastname": "Jaidee",
        "company": "Love2work",
        "salary": 20000
    },
    {
        "id": "1004",
        "firstname": "Monkey D",
        "lastname": "Luffee",
        "company": "One Piece",
        "salary": 9000000
    }
];

let limitCount = 0;
let rowData;
let htmlTag = '';
let headerRowLimit = 0;

for (let rowIndex in peopleSalary) {
    rowData = peopleSalary[rowIndex];

    // header
    if (headerRowLimit < 1) {
        htmlTag = '<tr>';
        for (let key in rowData) {
            if (key !== 'company') {
                if (limitCount < 4) {
                    htmlTag += `<th>${key}</th>`;
                    limitCount++;
                }
            }
        }
        htmlTag += '</tr>';
        headerRowLimit = 1;
    }

    // body
    for (let key in rowData) {
        if (key !== 'company') {
            htmlTag += `<td>${rowData[key]}</td>`;
        }
    }
    htmlTag += '</tr>';
}

// hw 3.2
let salarys;
htmlTag = '';

for (let rowIndex in peopleSalary) {
    rowData = peopleSalary[rowIndex];
    salarys = [];

    // calculate three years salary
    for (let key in rowData) {

        if (key === 'salary') {
            salarys.push(rowData[key]);

            for (let year = 0; year < 2; year++) {
                salarys.push(Math.floor(salarys[year] * 1.1));
            }
        }
    }

    // header
    if (headerRowLimit < 1) {
        htmlTag = '<tr>';
        for (let key in rowData) {
            if (key !== 'company') {
                if (limitCount < 4) {
                    htmlTag += `<th>${key}</th>`;
                    limitCount++;
                }
            }
        }
        htmlTag += '</tr>';
        headerRowLimit = 1;
    }

    // body

    // for (let i = 0; i < 3; i++) {
        htmlTag += '<tr>';
        for (let key in rowData) {
            if (key !== 'company') {
                if (key === 'salary') {
                    htmlTag += '<td><ol>';
                    for (let i = 0; i < 3; i++) {
                        htmlTag += `<li>${salarys[i]}</li>`;
                    }
                    htmlTag += '</td></ol>';
                } else {
                    htmlTag += `<td>${rowData[key]}</td>`;
                }
            }
        }
        htmlTag += '</tr>';
    // }
}
$('#peopleTable').append(htmlTag);













