package com.codecamp.day13;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class HomeServlet extends HttpServlet {
    final String KEY_USERNAME = "username";
    final String KEY_PASSWORD = "password";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalStateException {
        String usernameInput = req.getParameter("username");
        String passwordInput = req.getParameter("password");

        // get from json and set session
        ObjectMapper objectMapper = new ObjectMapper();
        String pathname = "WEB-INF/single_employee.json";
        String username = "";
        String password = "";

        InputStream stream = this.getServletContext().getResourceAsStream(pathname);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        reader.close();

        String jsonContent = stringBuilder.toString();

        HttpSession session = req.getSession();
//        session will expire in 30 seconds
        session.setMaxInactiveInterval(30);

        try {
            JsonNode jsonNode = objectMapper.readTree(jsonContent);
            username = jsonNode.get("username").textValue();
            password = jsonNode.get("password").textValue();

            session.setAttribute(KEY_USERNAME, URLEncoder.encode(username, "utf-8"));
            session.setAttribute(KEY_PASSWORD, URLEncoder.encode(password, "utf-8"));

        } catch (IOException e) {
            e.printStackTrace();
        }


        // validate username and password
        username = URLDecoder.decode((String) session.getAttribute(KEY_USERNAME), "utf-8");
        password = URLDecoder.decode((String) session.getAttribute(KEY_PASSWORD), "utf-8");

        session.setAttribute("isLoggedIn", false);
        if (usernameInput.equals(username) && passwordInput.equals(password)) {
            session.setAttribute("isLoggedIn", true);
            resp.sendRedirect("/member");
        } else {
            req.setAttribute("warning_text", "Username or Password is incorrect");
            req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
    }


}

