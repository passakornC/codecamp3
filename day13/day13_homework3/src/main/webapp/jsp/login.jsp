<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-21
  Time: 01:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<h2>${loggedinMsg}</h2>

<h2>${warnText}</h2>

<form method="POST" action="login">
    <p>
        <label>Username: </label>
        <input type="text" name="username" />
    </p>
    <p>
        <label>Password: </label>
        <input type="password" name="password" />
    </p>
    <p>
        <button type="submit">Login</button>
    </p>
</form>

</body>
</html>
