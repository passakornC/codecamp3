package com.codecamp.day13;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class LoginServlet extends HttpServlet {
    final String KEY_USERNAME = "username";
    final String KEY_PASSWORD = "password";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalStateException {
        String usernameInput = req.getParameter("username");
        String passwordInput = req.getParameter("password");

        // read from json file
        ObjectMapper objectMapper = new ObjectMapper();

        // หา path สำหรับ single_employee.json ในการใช้งานจริง วิธีนี้สามารถใช้ได้กับทุกเครื่อง
        ServletContext servletContext = req.getServletContext();

        try {
            URI uri = servletContext.getResource("WEB-INF/single_employee.json").toURI();
            Employee employee = objectMapper.readValue(new File(uri), Employee.class);

            // compare
            if (usernameInput.equals(employee.getUsername()) && passwordInput.equals(employee.getPassword())) {
                // save to cookie
//                Cookie cookie = new Cookie("username", usernameInput);
//                cookie.setMaxAge(30);
//                resp.addCookie(cookie);

                // save to session
                HttpSession session = req.getSession();
                session.setMaxInactiveInterval(30);
                session.setAttribute("username", usernameInput);
                resp.sendRedirect("member");
            } else {
                req.setAttribute("warnText", "Username/Password is incorrect.");
                req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // read value from cookie
//        Cookie[] cookieArray = req.getCookies();

        //        if (cookieArray != null) {
        //            for (Cookie cookie : cookieArray) {
        //                if (cookie.getName().equals("username")) {
        //                    req.setAttribute("loggedinMsg", "You are signed as " + cookie.getValue());
        //                }
        //            }
        //        }

        HttpSession httpSession = req.getSession();
        String usernameFromSession = (String) httpSession.getAttribute("username");

        if (usernameFromSession != null) {
            req.setAttribute("loggedinMsg", "You are signed as " + usernameFromSession);
        }

        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
    }


}

