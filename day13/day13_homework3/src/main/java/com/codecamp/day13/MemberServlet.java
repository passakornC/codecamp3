package com.codecamp.day13;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class MemberServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/member.jsp").forward(req, resp);
    }
}

