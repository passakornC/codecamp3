package com.codecamp.day13;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("datetimenow", URLEncoder.encode(new Date().toString(), "UTF-8"));
        resp.addCookie(cookie);

        Cookie[] cookies = req.getCookies();

        for (Cookie c : cookies) {
            if (c.getName().equals("datetimenow")) {
                req.setAttribute("datetimenow", URLDecoder.decode(c.getValue(), "UTF-8"));
            }
        }

        req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
    }


}

