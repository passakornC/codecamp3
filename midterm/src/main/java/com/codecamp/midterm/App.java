package com.codecamp.midterm;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Midterm CodeCamp3
 *
 */
public class App {
    private static String[] rawData = {
        "id:1001 firstname:Luke lastname:Skywalker",
        "id:1002 firstname:Tony lastname:Stark",
        "id:1003 firstname:Somchai lastname:Jaidee",
        "id:1004 firstname:Monkey D lastname:Luffee"
    };

    public static void main( String[] args ) {

        /*
         * create an arraylist of hashmap
         */
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        for (String rawDataLine : App.rawData) {
            HashMap<String, String> hashMap = new HashMap<>();
            String[] splitString = rawDataLine.split(" f");

            // id
            hashMap.put(splitString[0].split(":")[0], splitString[0].split(":")[1]);

            // firstname
            splitString[1] = "f" + splitString[1];
            splitString = splitString[1].split(" l");
            hashMap.put(splitString[0].split(":")[0], splitString[0].split(":")[1]);

            // lastname
            splitString[1] = "l" + splitString[1];
            hashMap.put(splitString[1].split(":")[0], splitString[1].split(":")[1]);

            // add the hashmap to the arraylist
            arrayList.add(hashMap);
        }

        /*
         * display the arraylist
         */
        for (HashMap<String ,String> hashMap : arrayList) {
            for (String key : hashMap.keySet()) {
                System.out.printf("%s: %s\n", key, hashMap.get(key));
            }
            System.out.println("----------------------------------------");
        }
    }
}
