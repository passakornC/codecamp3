package com.codecamp.day15;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Homework151Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String datasourceUrl = "jdbc:mysql://localhost:3306/bookstore";
        final String user = "root";
        final String password = "1234";

        try {
            // connect to database
            Connection connection = DriverManager.getConnection(datasourceUrl, user, password);

            // create a query command
            String sqlCommand = "SELECT firstname, lastname, age FROM employee;";
            Statement statement = connection.createStatement();

            // get result
            ResultSet resultSet = statement.executeQuery(sqlCommand);

            // display the result
            // employee table
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
            while (resultSet.next()) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("First name", resultSet.getString("firstname"));
                hashMap.put("Last name", resultSet.getString("lastname"));
                hashMap.put("Age", resultSet.getString("age"));

                arrayList.add(hashMap);
            }
            req.setAttribute("employeeList", arrayList);

            // book table
            sqlCommand = "SELECT isbn, title, price FROM book;";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sqlCommand);
            ArrayList<Book> bookList = new ArrayList<>();
            while (resultSet.next()) {
                Book book = new Book();
                book.setIsbn(resultSet.getString("isbn"));
                book.setTitle(resultSet.getString("title"));
                book.setPrice(resultSet.getInt("price"));

                bookList.add(book);
            }
            req.setAttribute("bookList", bookList);

            req.getRequestDispatcher("jsp/homework151.jsp").forward(req, resp);

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
