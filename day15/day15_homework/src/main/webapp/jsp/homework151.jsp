<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-26
  Time: 13:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <style>
        table, table th, table td {
            border: 1px solid black;
        }
    </style>
</head>
<body>

<%--Employee Table--%>
<table>
    <thead>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Age</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${employeeList}" var="employee">
            <tr>
                <td><c:out value="${employee[\"First name\"]}" /></td>
                <td><c:out value="${employee[\"Last name\"]}" /></td>
                <td><c:out value="${employee.Age}" /></td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<br />
<br />

<%--Book Table--%>
<table>
    <thead>
    <tr>
        <th>ISBN</th>
        <th>Title</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${bookList}" var="book">
        <tr>
            <td><c:out value="${book.getIsbn()}" /></td>
            <td><c:out value="${book.getTitle()}" /></td>
            <td><c:out value="${book.getPrice()}" /></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
