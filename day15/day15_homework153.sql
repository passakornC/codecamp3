USE bookstore;

SELECT * FROM bookstore.book;

# ค้นหาหนังสือจากชื่อ เช่น The Marquis and I ให้ค้นว่า Mar ก็ต้องหาเจอ
SELECT title FROM bookstore.book WHERE title LIKE '%Mar%';

# ดึงหนังสือมาแสดง 2 เล่มแรก ที่ชื่อมีตัวอักษร o
SELECT title FROM bookstore.book WHERE title LIKE '%o%' LIMIT 2;

# เพิ่มรายการขายหนังสือ 10 รายการ
INSERT INTO bookstore.sale_detail (isbn, employee_id, price, quantity, created_at)
VALUES ('9783598215933', 1, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215933'), 1, NOW()),
       ('9783598215933', 2, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215933'), 2, NOW()),
       ('9783598215957', 3, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215957'), 3, NOW()),
       ('9783598215957', 4, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215957'), 4, NOW()),
       ('9783598215995', 1, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215995'), 5, NOW()),
       ('9783598215995', 2, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215995'), 6, NOW()),
       ('9783598215919', 3, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215919'), 7, NOW()),
       ('9783598215919', 4, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215919'), 8, NOW()),
       ('9783598215766', 1, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215766'), 9, NOW()),
       ('9783598215766', 2, (SELECT price FROM bookstore.book WHERE book.isbn = '9783598215766'), 10, NOW());

SELECT * FROM sale_detail;

# หาว่าขายหนังสือได้กี่เล่ม
SELECT SUM(bookstore.sale_detail.quantity) FROM bookstore.sale_detail;

# หาว่ามีหนังสืออะไรบ้างที่ขายออก (แสดงแค่ ISBN ของหนังสือ)
SELECT DISTINCT isbn FROM bookstore.sale_detail;

# หาว่าขายหนังสือได้เงินทั้งหมดเท่าไร
SELECT SUM(price * quantity) FROM bookstore.sale_detail;

