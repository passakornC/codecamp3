CREATE DATABASE IF NOT EXISTS dub_bookstore COLLATE utf8mb4_general_ci;

USE dub_bookstore;

CREATE TABLE IF NOT EXISTS book AS SELECT * FROM bookstore.book;

CREATE TABLE IF NOT EXISTS employee AS SELECT * FROM bookstore.employee;

SELECT * FROM employee;

# The fifth employee resigned
DELETE FROM employee WHERE id = 5;

# Add address column
ALTER TABLE employee ADD COLUMN address VARCHAR(255);

DESC employee;

# Total number of employee
SELECT COUNT(*) AS "Number of Employee" FROM employee;

# Employee's age less than 20
SELECT * FROM employee WHERE age < 20;

# Display firstname and age of all employees
SELECT firstname AS "First name", age AS "Age" FROM employee;

# Noah -> Neil
UPDATE employee SET firstname = "Neil" WHERE firstname = "Noah";