package com.codecamp.day8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Homework83 {
    public static void main(String[] args) {
        Homework83 homework83 = new Homework83();
        homework83.runHomework83();
    }

    private void runHomework83() {
        ArrayList<Animal> zooAnimal = new ArrayList<>();
        int tigerAmount = 3;
        int penguinAmount = 5;
        int hippoAmount = 4;
        for(int i = 0; i < tigerAmount; i++) {
            zooAnimal.add(new Animal("Tiger"));
        }
        for(int i = 0; i < penguinAmount; i++) {
            zooAnimal.add(new Animal("Penguin"));
        }
        for(int i = 0; i < hippoAmount; i++) {
            zooAnimal.add(new Animal("Hippo"));
        }

        // Continue your code here
        Animal animal;
        Iterator<Animal> iterator = zooAnimal.iterator();
        HashMap<String, Integer> countList = new HashMap<>();
        int currentCount;

        // Count loop
        while (iterator.hasNext()) {
            animal = iterator.next();
            if (animal.getName() == "Tiger") {
                if (countList.containsKey("Tiger")) {
                    currentCount = countList.get("Tiger");
                    countList.put("Tiger", ++currentCount);
                } else {
                    countList.put("Tiger", 1);
                }
            } else if (animal.getName() == "Penguin") {
                if (countList.containsKey("Penguin")) {
                    currentCount = countList.get("Penguin");
                    countList.put("Penguin", ++currentCount);
                } else {
                    countList.put("Penguin", 1);
                }
            } else {
                if (countList.containsKey("Hippo")) {
                    currentCount = countList.get("Hippo");
                    countList.put("Hippo", ++currentCount);
                } else {
                    countList.put("Hippo", 1);
                }
            }
        }

        // Display result
        for (String animalName : countList.keySet()) {
            System.out.println(animalName + " : " + countList.get(animalName));
        }
    }
}
