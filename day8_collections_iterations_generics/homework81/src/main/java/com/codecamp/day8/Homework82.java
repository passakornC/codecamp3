package com.codecamp.day8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class Homework82 {
    private static String[] rawData = {
        "id:1001 firstname:Luke lastname:Skywalker",
        "id:1002 firstname:Tony lastname:Stark",
        "id:1003 firstname:Somchai lastname:Jaidee",
        "id:1004 firstname:Monkey D lastname:Luffee"
    };

    public static void main(String[] args) {
        ArrayList<HashMap<String, String>> list = new ArrayList<>();
        HashMap<String, String> hashMap;
        String[] afterStringSplitArray;

        ArrayList<String> individual = new ArrayList<>();

        for (String stringLine : rawData) {
            ArrayList<String> afterStringSplitList = new ArrayList<>(Arrays.asList(stringLine.split(" l")));
            individual.add('l' + afterStringSplitList.get(1));

            afterStringSplitList.addAll(Arrays.asList(afterStringSplitList.get(0).split(" f")));
            individual.add(afterStringSplitList.get(0));
            individual.add('f' + afterStringSplitList.get(1));

            hashMap = new HashMap<>();
            for (String keyAndValue : individual) {
                afterStringSplitArray = keyAndValue.split(":");
                hashMap.put(afterStringSplitArray[0], afterStringSplitArray[1]);
            }
            list.add(hashMap);

            // Display
            Iterator<HashMap<String, String>> iterator = list.iterator();
            while (iterator.hasNext()) {
                hashMap = iterator.next();
                System.out.printf("id: %s \nname: %s %s\n", hashMap.get("id"), hashMap.get("firstname"), hashMap.get("lastname"));
            }
        }


    }
}
