package com.codecamp.day8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;

public class Homework81 {
    public static void main( String[] args ) {
        Homework81 homework81 = new Homework81();
        homework81.run();
    }

    private void run() {
        // 1.1
        String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker",
            "id:1002 firstname:Tony lastname:Stark",
            "id:1003 firstname:Somchai lastname:Jaidee",
            "id:1004 firstname:Monkey D lastname:Luffee"
        };

        ArrayList<HashMap<String, String>> people = new ArrayList<>();
        HashMap<String, String> person;
        String[] stringArray;
        ArrayList<String> splitString;
        ArrayList<String> individual;

        for (String stringLine : rawData) {
            individual = new ArrayList<>();
            stringArray = stringLine.split(" l");
            splitString = new ArrayList<>(Arrays.asList(stringArray));
            String idAndFirstnameString = splitString.get(0);
            individual.add('l' + splitString.get(1));

            stringArray = idAndFirstnameString.split(" f");
            splitString = new ArrayList<>(Arrays.asList(stringArray));
            individual.add(splitString.get(0));
            individual.add('f' + splitString.get(1));

            person = new HashMap<>();
            for (String keyAndValue : individual) {
                stringArray = keyAndValue.split(":");
                person.put(stringArray[0], stringArray[1]);
            }
            people.add(person);
        }

        // 1.2
        String string;
        for (HashMap<String, String> p : people) {
            string = "";
            string += "id: " + p.get("id") + "\n";
            string += "name: " + p.get("firstname") + " " + p.get("lastname") + "\n";
            System.out.print(string);
        }
    }

}
