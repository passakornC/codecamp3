<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-20
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${pageTitle}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/jsp/style.css" type="text/css">
</head>
<body>
<div class="container">
    <nav class="menu">
        <ul>
            <li><a href="${pageContext.request.contextPath}/home/">About Me</a> |</li>
            <li><a href="${pageContext.request.contextPath}/skill/">Skill</a> |</li>
            <li><a href="${pageContext.request.contextPath}/contactme/">Contact Me</a> |</li>
            <li><a href="${pageContext.request.contextPath}/portfolio/">Portfolio</a></li>
        </ul>
    </nav>
    <div class="portfolio-header">
        <div class="page-title-container">
            <h1 class="page-title">About Me</h1>
        </div>
        <div class="profile-image"></div>
    </div>
    <div class="portfolio-detail">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, aspernatur consectetur deleniti
            exercitationem incidunt labore magni mollitia nobis officiis quae quaerat recusandae sint sit tenetur
            vel velit veniam voluptates voluptatum!</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus deleniti eius enim fugit iste itaque
            magnam magni maxime minus modi nulla omnis quam quo, reprehenderit tempore, ullam, veritatis? Facilis,
            illum!</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci corporis dolorem error facere fugiat iusto
            magni maxime, minus nam nobis praesentium, quia recusandae saepe sed similique tenetur veritatis! Assumenda,
            hic!</p>
    </div>
</div>
</body>
</html>
