<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-20
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${pageTitle}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/jsp/style.css" type="text/css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <!-- Navigation -->
    <nav class="menu">
        <ul>
            <li><a href="${pageContext.request.contextPath}/home/">About Me</a> |</li>
            <li><a href="${pageContext.request.contextPath}/skill/">Skill</a> |</li>
            <li><a href="${pageContext.request.contextPath}/contactme/">Contact Me</a> |</li>
            <li><a href="${pageContext.request.contextPath}/portfolio/">Portfolio</a></li>
        </ul>
    </nav>
    <!-- End navigation -->

    <!-- Header -->
    <header class="search-container">
        <div class="portfolio-title">
            <div class="portfolio-image"></div>
            <div class="portfolio-name">
                <span>My Application</span>
            </div>
        </div>
        <div class="search-group">
            <div class="search-group-inner right-addon">
                <input type="text" class="search-input-box" placeholder="Search Product">
                <i class="glyphicon glyphicon-search"></i>
                <button class="search-button">Search</button>
            </div>
        </div>
        <div class="new-product">
            <div><a href="#">New Product</a></div>
        </div>
    </header>
    <!-- End header -->

    <!-- Content -->
    <div class="content-container">
        <div class="content-item">
            <div class="content-image"></div>
            <div class="content-detail">
                <ul>
                    <li>List-1</li>
                    <li>List-2</li>
                    <li>List-3</li>
                </ul>
            </div>
        </div>
        <div class="content-item">
            <div class="content-image"></div>
            <div class="content-detail">
                <ul>
                    <li>List-1</li>
                    <li>List-2</li>
                    <li>List-3</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-container">
        <div class="content-item">
            <div class="content-image"></div>
            <div class="content-detail">
                <ul>
                    <li>List-1</li>
                    <li>List-2</li>
                    <li>List-3</li>
                </ul>
            </div>
        </div>
        <div class="content-item">
            <div class="content-image"></div>
            <div class="content-detail">
                <ul>
                    <li>List-1</li>
                    <li>List-2</li>
                    <li>List-3</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-container">
        <div class="content-item">
            <div class="content-image"></div>
            <div class="content-detail">
                <ul>
                    <li>List-1</li>
                    <li>List-2</li>
                    <li>List-3</li>
                </ul>
            </div>
        </div>
        <div class="content-item">
            <div class="content-image"></div>
            <div class="content-detail">
                <ul>
                    <li>List-1</li>
                    <li>List-2</li>
                    <li>List-3</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End content -->
</div>
</body>
</html>
