<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-20
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${pageTitle}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/jsp/style.css" type="text/css">
</head>
<body>
<div class="container">
    <nav class="menu">
        <ul>
            <li><a href="${pageContext.request.contextPath}/home/">About Me</a> |</li>
            <li><a href="${pageContext.request.contextPath}/skill/">Skill</a> |</li>
            <li><a href="${pageContext.request.contextPath}/contactme/">Contact Me</a> |</li>
            <li><a href="${pageContext.request.contextPath}/portfolio/">Portfolio</a></li>
        </ul>
    </nav>
    <div class="portfolio-header">
        <div class="page-title-container">
            <h1 class="page-title">Contact Me</h1>
        </div>
    </div>
    <div class="portfolio-detail">
        <ul class="address">
            <li>Phone: +66-0123456789</li>
            <li>Email: myemail@xmail.com</li>
            <li>Line ID: mylineid</li>
            <li>Address: 123 This Street, Bkk, 10100</li>
        </ul>
    </div>
</div>
</body>
</html>
