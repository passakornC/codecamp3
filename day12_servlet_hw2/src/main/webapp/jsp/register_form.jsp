<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register Form</title>
    <style>
        body {
            display: flex;
            justify-content: center;
        }

        .form-group {
            width: 800px;
        }

        .container {
            display: flex;
            margin: 0 auto;
            width: 70%;
        }

        .button-container {
            display: flex;
            justify-content: center;
        }

        .container .key,
        .container .value {
            display: flex;
            align-items: center;
            flex: 1 1 50%;
        }

        .container .value {
            margin: 2px auto;
        }

        .container .value input,
        .container .key input {
            font-size: 1.2em;
        }

        .button-container .button {
            margin: 5px;
        }

        .container #pwd-warning {
            color: red;
            font-weight: bold;
        }

    </style>
</head>
<body>
<form action="" method="post" id="regform">
    <fieldset class="form-group">
        <legend>Register Form</legend>
        <!-- Firstname -->
        <div class="container">
            <div class="key">
                <label for="firstname">Firstname: </label>
            </div>
            <div class="value">
                <input id="firstname" type="text" name="firstname">
            </div>
        </div>
        <!-- End Firstname -->

        <!-- Lastname -->
        <div class="container">
            <div class="key">
                <label for="lastname">Lastname: </label>
            </div>
            <div class="value">
                <input id="lastname" type="text" name="lastname">
            </div>
        </div>
        <!-- End Lastname -->

        <!-- Password -->
        <div class="container">
            <div class="key">
                <label for="password">Password: </label>
                <span id="pwd-warning"></span>
            </div>
            <div class="value">
                <input id="password" type="password" name="password">
            </div>
        </div>
        <!-- End Password -->

        <!-- Confirm password -->
        <div class="container">
            <div class="key">
                <label for="confirmpassword">Confirm Password: </label>
            </div>
            <div class="value">
                <input id="confirmpassword" type="password" name="confirmpassword">
            </div>
        </div>
        <!-- End Confirm password -->

        <!-- Email -->
        <div class="container">
            <div class="key">
                <label for="email">Email: </label>
            </div>
            <div class="value">
                <input id="email" type="email" name="email">
            </div>
        </div>
        <!-- End Email -->

        <!-- Confirm Email -->
        <div class="container">
            <div class="key">
                <label for="confirmemail">Confirm Email: </label>
            </div>
            <div class="value">
                <input id="confirmemail" type="email" name="confirmemail">
            </div>
        </div>
        <!-- End Email password -->

        <!-- Birthday -->
        <div class="container">
            <div class="key">
                <p>Birthday: </p>
            </div>
            <div class="value">
                <select name="birthday" id="birthday">
                    <c:forEach begin="1" end="31" step="1" var="number">
                        <option value="<c:out value="${number}" />"><c:out value="${number}" /></option>
                    </c:forEach>
                </select>
                <select name="birthmonth" id="birthmonth">
                    <c:forEach begin="1" end="12" step="1" var="number">
                        <option value="<c:out value="${number}" />"><c:out value="${number}" /></option>
                    </c:forEach>
                </select>
                <select name="birthyear" id="birthyear">
                    <c:forEach begin="1980" end="2005" step="1" var="number">
                        <option value="<c:out value="${number}" />"><c:out value="${number}" /></option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <!-- End Birthday -->

        <!-- Gender -->
        <div class="container">
            <div class="key">
                <p>Gender: </p>
            </div>
            <div class="value">
                <input id="gender-male" type="radio" name="gender" checked="checked">
                <label for="gender-male">Male: </label>

                <input id="gender-female" type="radio" name="gender">
                <label for="gender-female">Female: </label>
            </div>
        </div>
        <!-- End Gender -->

        <!-- Submit & reset -->
        <div class="button-container">
            <div class="key button">
                <input type="button" name="submit_button" value="Submit" onclick="check();">
            </div>
            <div class="value button">
                <input type="reset" name="reset" value="Reset">
            </div>
        </div>
        <!-- End Submit & reset -->
    </fieldset>
</form>
<%--<script>--%>
    <%--// Check if an email and a password are the same--%>
    <%--function check() {--%>
        <%--let password = document.getElementById('password').value;--%>
        <%--let email = document.getElementById('email').value;--%>
        <%--let element = document.getElementById('pwd-warning');--%>
        <%--let regForm = document.getElementById('regform');--%>

        <%--if (password === email) {--%>
            <%--element.innerText = 'Password and Email are the same !!!';--%>

            <%--setTimeout(function() {--%>
                <%--regForm.submit();--%>
            <%--}, 2000);--%>
        <%--} else {--%>
            <%--regForm.submit();--%>
        <%--}--%>
    <%--}--%>

<%--</script>--%>
</body>
</html>
