package com.codecamp.day12;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


//        req.setAttribute("getServletPath", req.getServletPath());
//        req.setAttribute("getPathTranslated", req.getPathTranslated());
//        req.setAttribute("getRequestURL", req.getRequestURL());
//        req.setAttribute("getContextPath", req.getContextPath());
//        req.setAttribute("getPathInfo", req.getPathInfo());
//        req.setAttribute("getRequestURI", req.getRequestURI());
//
//        req.getRequestDispatcher("/jsp/temp.jsp").forward(req, resp);
//
//

//        String url = req.getRequestURI();
        String url = req.getServletPath();

        if (url.equals("/skill/") || url.equals("/skill")) {
            req.setAttribute("pageTitle", "Skill");
            req.getRequestDispatcher("/jsp/pages/skill.jsp").forward(req, resp);
        } else if (url.equals("/contactme/") || url.equals("/contactme")) {
            req.setAttribute("pageTitle", "Contact Me");
            req.getRequestDispatcher("/jsp/pages/contactme.jsp").forward(req, resp);
        } else if (url.equals("/portfolio/") || url.equals("/portfolio")) {
            req.setAttribute("pageTitle", "Portfolio");
            req.getRequestDispatcher("/jsp/pages/portfolio.jsp").forward(req, resp);
        } else if (url.equals("/register") || url.equals("/register/")) {
            req.setAttribute("pageTitle", "Register Form");
            req.getRequestDispatcher("/jsp/register_form.jsp").forward(req, resp);
        } else {
            req.setAttribute("pageTitle", "About Me");
            req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
        }
    }

}

