<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-25
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>Student Id</th>
        <th>First name</th>
        <th>Last name</th>
    </tr>
    </thead>

    <c:forEach items="${studentList}" var="student">
        <tr>
            <td><c:out value="${student.studentId}" /></td>
            <td><c:out value="${student.firstname}" /></td>
            <td><c:out value="${student.lastname}" /></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
