package com.codecamp.day14;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalStateException {
        String usernameInput = req.getParameter("username");
        String passwordInput = req.getParameter("password");


        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp", "root", "1234");
//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery("SELECT username, password FROM codecamp.user WHERE username = " + usernameInput);
            String sqlCommand = "SELECT username, password FROM user WHERE username = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand);
            preparedStatement.setString(1, usernameInput);
            ResultSet resultSet = preparedStatement.executeQuery();
            String usernameDb = "";
            String passwordDb = "";
            final int FETCH_SIZE = resultSet.getFetchSize();

            while (resultSet.next()) {
                usernameDb = resultSet.getString("username");
                passwordDb = resultSet.getString("password");
            }
//            statement.close();


            // compare
            if (!usernameInput.equals("") && !passwordInput.equals("") &&
                usernameInput.equals(usernameDb) && passwordInput.equals(passwordDb)) {
                // save to session
                HttpSession session = req.getSession();
                session.setMaxInactiveInterval(30);
                session.setAttribute("username", usernameInput);
                resp.sendRedirect("member");
            } else {
                req.setAttribute("warnText", "Username/Password is incorrect.");
                req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession httpSession = req.getSession();
        String usernameFromSession = (String) httpSession.getAttribute("username");

//        if (usernameFromSession != null) {
            req.setAttribute("loggedinMsg", "You are signed as " + usernameFromSession);
//        }

        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
    }


}

