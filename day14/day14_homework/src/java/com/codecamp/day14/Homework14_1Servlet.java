package com.codecamp.day14;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class Homework14_1Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalStateException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp", "root", "1234");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM student WHERE studentid != \"0001\"");
            ArrayList<Student> studentList = new ArrayList<>();

            while (resultSet.next()) {
                String studentid = resultSet.getString("studentid");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");

                Student student = new Student();
                student.setStudentId(studentid);
                student.setFirstname(firstname);
                student.setLastname(lastname);

                studentList.add(student);
            }
            statement.close();
            connection.close();

            req.setAttribute("studentList", studentList);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        req.getRequestDispatcher("/jsp/homework141.jsp").forward(req, resp);
    }

}

