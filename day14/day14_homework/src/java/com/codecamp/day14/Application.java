package com.codecamp.day14;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

public class Application {
    public static void main(String[] args) throws LifecycleException {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir("temp");
        tomcat.setPort(8085);

        tomcat.addWebapp("", new File("src/main/webapp").getAbsolutePath());
        tomcat.getConnector();
        tomcat.start();
    }
}
