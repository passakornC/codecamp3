import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Task} from '../../models/task';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  /**
   * Property
   */
  @Input('taskInTaskComponent') private taskInTaskComponent: Task;
  @Input('isSelected') private isSelected: boolean;
  @Input('taskIdToHilight') private taskIdToHilight: number;
  @Output('taskToRemove') private taskToRemove: EventEmitter<Task>;
  @Output('taskToShowDetail') private taskToShowDetail: EventEmitter<Task>;


  /**
   * Constructor
   */
  constructor() {
    this.taskToRemove = new EventEmitter<Task>();
    this.taskToShowDetail = new EventEmitter<Task>();
    this.isSelected = false;
  }


  /**
   * Methods
   */
  ngOnInit() {
  }

  removeTaskObj() {
    this.taskToRemove.emit(this.taskInTaskComponent);
  }

  selectTask() {
    this.taskToShowDetail.emit(this.taskInTaskComponent);
  }
}
