import {Component, OnChanges, OnInit, ViewChild} from '@angular/core';
import {TaskComponent} from '../task/task.component';
import {Task} from '../../models/task';
import {NULL_EXPR, nullSafeIsEquivalent} from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  /**
   * Property
   */
  private static taskIdList: number;
  private static selectedTaskKeeper: Task;
  taskList: Array<Task>;
  selectedTaskKeeper: Task;
  shouldHilight: boolean; // if true, hi-light that task
  taskIdToHilight: number;


  /**
   * Constructor
   */
  constructor() {
    TodoListComponent.taskIdList = 0;
    TodoListComponent.selectedTaskKeeper = null;
    this.selectedTaskKeeper = null;
    this.shouldHilight = false;
    this.taskIdToHilight = -1;

    // intial task
    const task1: Task = new Task(TodoListComponent.getTaskId(), 'Task 1', 'Task 1 description');
    this.taskList = [task1];
  }


  /**
   * Methods
   */
  private static getTaskId(): number {
    return ++TodoListComponent.taskIdList;
  }

  ngOnInit() {
  }

  addTask(titleInput: string, descInput: string) {
    const task: Task = new Task(TodoListComponent.getTaskId(), titleInput, descInput);
    this.taskList.push(task);
  }

  removeTask(taskToRemove: Task) {
    const index: number = this.taskList.indexOf(taskToRemove);
    this.taskList.splice(index, 1);
  }

  showTaskDetail(selectedTask: Task) {
    if (TodoListComponent.selectedTaskKeeper !== selectedTask) {
      TodoListComponent.selectedTaskKeeper = selectedTask;
      this.shouldHilight = true;
    } else {
      // select the same task for the second time => toggle
      TodoListComponent.selectedTaskKeeper = new Task();
      this.shouldHilight = false;
    }

    this.selectedTaskKeeper = TodoListComponent.selectedTaskKeeper;
    this.taskIdToHilight = this.selectedTaskKeeper.id;
  }

}
