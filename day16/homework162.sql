# Homework162

USE day16;

# เพิ่ม student ลงไป 10 คน
INSERT INTO students (name)
VALUES ('Student-1'),
       ('Student-2'),
       ('Student-3'),
       ('Student-4'),
       ('Student-5'),
       ('Student-6'),
       ('Student-7'),
       ('Student-8'),
       ('Student-9'),
       ('Student-10');




# ให้ student ลงเรียนแต่ละวิชาที่แตกต่างกัน (อาจจะซ้ำกันบ้าง)
INSERT INTO enrolls (student_id, course_id)
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 3),
       (5, 4),
       (6, 5),
       (7, 5),
       (8, 5),
       (9, 6),
       (10, 7);

# มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ)
SELECT DISTINCT c.name FROM courses c
LEFT JOIN enrolls e ON c.id = e.course_id
WHERE e.student_id IS NOT NULL
ORDER BY c.name ASC;

# มีคอร์สไหนบ้างที่ไม่มีคนเรียน
SELECT DISTINCT c.name FROM courses c
LEFT JOIN enrolls e ON c.id = e.course_id
WHERE e.student_id IS NULL
ORDER BY c.name ASC;
