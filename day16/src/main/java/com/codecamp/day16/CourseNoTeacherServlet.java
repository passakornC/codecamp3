package com.codecamp.day16;

import com.mysql.cj.jdbc.Driver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class CourseNoTeacherServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String databaseURL = "jdbc:mysql://localhost:3306/day16";
        final String databaseUser = "root";
        final String databasePassword = "1234";
        String sqlCommand = "select c.name course_name from courses c " +
                "left join instructors i on c.teach_by = i.id " +
                "where i.name is null order by c.name asc";

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlCommand);

            ArrayList<String> arrayList = new ArrayList<>();
            while (resultSet.next()) {
                arrayList.add(resultSet.getString("course_name"));
            }
            req.setAttribute("courseList", arrayList);
            req.getRequestDispatcher("jsp/course-no-teacher.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
