<%--
  Created by IntelliJ IDEA.
  User: passakornchoosuk
  Date: 2019-03-27
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<ul>
    <c:forEach items="${courseList}" var="course">
        <li><c:out value="${course}" /></li>
    </c:forEach>
</ul>
</body>
</html>
