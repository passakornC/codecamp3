fetch('data/data.json')
    .then(response => response.json())
    .then(employees => {
        let newEmployees = addAdditionalFields(employees);

        // change salary employee[0]
        newEmployees[0]['salary'] = 0;

        displayResult(employees);
        displayResult(newEmployees, '#new-employees');
    });


/*
 * Functions
 */
function addYearSalary(employee) {
    employee['yearSalary'] = employee['salary'] * 12;
}

function addNextYearSalary(employee) {
    let salarys = new Array(3);
    const maxYear = 3;

    for (let year = 0; year < maxYear; year++) {
        if (year === 0) { // only first year
            salarys[year] = employee['salary'];
        } else { // next year increase 10%
            salarys[year] = Math.floor(salarys[year - 1] * 1.1);
        }
    }

    // add new field
    employee['nextSalary'] = salarys;
}

function addAdditionalFields(people) {
    let newEmployees = cloneObject(people);

    for (let index in newEmployees) {
        addYearSalary(newEmployees[index]);
        addNextYearSalary(newEmployees[index]);
    }

    return newEmployees;
}

function displayResult(data, tableId='#employees') {
    let tableTag = createTaleHeader(data) + createTableBody(data);
    $(tableId).append(tableTag);
}

function createTaleHeader(data) {
    let headerTag = '';

    headerTag += '<thead><tr>';
    for (let index in Object.keys((data[0]))) {
        headerTag += `<th>${Object.keys(data[0])[index]}</th>`;
    }
    headerTag += '</tr></thead>';

    return headerTag;
}

function createTableBody(data) {
    let bodyTag = '';
    let employee;

    bodyTag += '<tbody>';
    for (let index in data) {
        employee = data[index];

        bodyTag += '<tr>';
        for (let key in employee) {
            bodyTag += '<td>';
            if (key === 'nextSalary') {
                bodyTag += '<ol>';
                for (let index in employee['nextSalary']) {
                    bodyTag += `<li>${employee['nextSalary'][index]}</li>`;
                }
                bodyTag += '</ol>';
            } else {
                bodyTag += `${employee[key]}`;
            }
        }
        bodyTag += '</tr>';
    }
    bodyTag += '</tbody>';

    return bodyTag;
}

function cloneObject(originalObj) {
    let cloneData = [];
    let cloneObj;
    let nextSalary;
    let keys = Object.keys(originalObj[0]);
    let values;

    for (let employeeIndex in originalObj) {
        cloneObj = {};
        nextSalary = [];
        values = Object.values(originalObj[employeeIndex]);

        for (let index in keys) {
            if (keys[index] !== 'nextSalary') {
                cloneObj[keys[index]] = values[index];
            } else {
                for (let salaryIndex in values[index]) {
                    nextSalary.push(values[index][salaryIndex]);
                }
                cloneObj[keys[index]] = nextSalary;
            }
        }

        cloneData[employeeIndex] = cloneObj;
    }

    return cloneData;
}
