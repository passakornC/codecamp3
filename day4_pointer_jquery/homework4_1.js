fetch('data/data.json')
    .then(response => response.json())
    .then(data => {
        addAdditionalFields(data);
        displayResult(data);
    });


/*
 * Functions
 */
function addYearSalary(employee) {
    employee['yearSalary'] = employee['salary'] * 12;
}

function addNextYearSalary(employee) {
    let salarys = new Array(3);
    const maxYear = 3;

    for (let year = 0; year < maxYear; year++) {
        if (year === 0) { // only first year
            salarys[year] = employee['salary'];
        } else { // next year increase 10%
            salarys[year] = Math.floor(salarys[year - 1] * 1.1);
        }
    }

    // add new field
    employee['nextSalary'] = salarys;
}

function addAdditionalFields(people) {
    for (let index in people) {
        addYearSalary(people[index]);
        addNextYearSalary(people[index]);
    }
}

function displayResult(data) {
    let tableTag = createTaleHeader(data) + createTableBody(data);
    $('#people').append(tableTag);
}

function createTaleHeader(data) {
    let headerTag = '';

    headerTag += '<thead><tr>';
    for (let index in Object.keys((data[0]))) {
        headerTag += `<th>${Object.keys(data[0])[index]}</th>`;
    }
    headerTag += '</tr></thead>';

    return headerTag;
}

function createTableBody(data) {
    let bodyTag = '';
    let employee;

    bodyTag += '<tbody>';
    for (let index in data) {
        employee = data[index];

        bodyTag += '<tr>';
        for (let key in employee) {
            bodyTag += '<td>';
            if (key === 'nextSalary') {
                bodyTag += '<ol>';
                for (let index in employee['nextSalary']) {
                    bodyTag += `<li>${employee['nextSalary'][index]}</li>`;
                }
                bodyTag += '</ol>';
            } else {
                bodyTag += `${employee[key]}`;
            }
        }
        bodyTag += '</tr>';
    }
    bodyTag += '</tbody>';

    return bodyTag;
}
