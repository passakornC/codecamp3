export class Ai implements ICockroachKiller, ICleaner {
    /**
     * Property
     */
    private _name: string;
    private _language: string;
    private _number: number;
    private _baygonNumber: number;
    private _building: string;
    private _cleanedRoomName: string[];
    private _toolName: Tool;
    private static _numberOfKilled: number;
    private static _cockroachKilledList: Array<{[key: string]: string}>;

    /**
     * Constructor
     */
    constructor();
    constructor(name?: string, language?: string) {
        Ai._numberOfKilled = 0;
        this._name = name;
        this._language = language;
    }


    /**
     * Method from ICockroachKiller
     */
    buyBaygon(amount: number) {
        console.log(`Buy " + ${amount} + " baygon(s).`);
    }

    killCockroach(baygonNumber: number, building: string, roomName: string) {
        Ai._numberOfKilled += 100;
        Ai._cockroachKilledList.push({"building": building, "roomName": roomName});
        console.log("Ai uses " + baygonNumber + " to kill cockroach in " + roomName + " of " + building);
    }

    getTotalKilled(): number {
        return Ai._numberOfKilled;
    }


    /**
     * Method from ICleaner
     */
    setTools(toolName: Tool) {
        this._toolName = toolName;
    }

    clean(building: string, roomName: string) {
        console.log("Ai cleans at " + roomName + " in " + building);
    }

    getCleanedRoom(): Array<{[key: string]: string}> {
        return Ai._cockroachKilledList;
    }


    /**
     * Getter
     */


    get name(): string {
        return this._name;
    }

    get language(): string {
        return this._language;
    }

    get number(): number {
        return this._number;
    }

    get baygonNumber(): number {
        return this._baygonNumber;
    }

    get building(): string {
        return this._building;
    }

    get cleanedRoomName(): string[] {
        return this._cleanedRoomName;
    }

    get toolName(): Tool {
        return this._toolName;
    }

    static get numberOfKilled(): number {
        return this._numberOfKilled;
    }

    static get cockroachKilledList(): Array<{ [p: string]: string }> {
        return this._cockroachKilledList;
    }

    /**
     * Setter
     */

    set name(value: string) {
        this._name = value;
    }

    set language(value: string) {
        this._language = value;
    }

    set number(value: number) {
        this._number = value;
    }

    set baygonNumber(value: number) {
        this._baygonNumber = value;
    }

    set building(value: string) {
        this._building = value;
    }

    set cleanedRoomName(value: string[]) {
        this._cleanedRoomName = value;
    }

    set toolName(value: Tool) {
        this._toolName = value;
    }

    static set numberOfKilled(value: number) {
        this._numberOfKilled = value;
    }

    static set cockroachKilledList(value: Array<{ [p: string]: string }>) {
        this._cockroachKilledList = value;
    }
}
