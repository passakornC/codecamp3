///<reference path="employee.ts"/>
import {Employee} from './employee';
import {ICockroachKiller} from './iCockroachKiller';
import {ICleaner} from "./iCleaner";


export class OfficeCleaner extends Employee implements ICockroachKiller, ICleaner {
    /**
     * Property
     */
    // private _dressCode: string;
    private _toolName: Tool;
    private static _numberOfKilled: number;
    private static _cockroachKilledList: Array<{[key: string]: string}>;


    /**
     * Constructor
     */
    constructor(firstName: string, lastName: string, salary: number);
    constructor(firstName: string, lastName: string, salary: number, dressCode?: string) {
        super(firstName, lastName, salary);
        // @ts-ignore
        super.dressCode = dressCode;
        OfficeCleaner._numberOfKilled = 0;
    }


    /**
     * Method
     */
    public static decorateRoom() {
        console.log("DocorateRoom");
    }

    public static welcomeGuest() {
        console.log("WelcomeGuest");
    }

    work() {
        this.clean("Building A", "Room A");
        this.killCockroach(99, "Building A", "Room A");
        OfficeCleaner.decorateRoom();
        OfficeCleaner.welcomeGuest();
    }


    /**
     * Method from ICockroachKiller
     */
    buyBaygon(amount: number) {}

    getTotalKilled(): number {
        return OfficeCleaner._numberOfKilled;
    }

    killCockroach(amount: number, building: string, roomName: string) {
        OfficeCleaner._numberOfKilled += 100;
        OfficeCleaner._cockroachKilledList.push({"building": building, "roomName": roomName});
        console.log("Office cleaner uses " + amount + " baygons to kill cockroaches in " + roomName + " at " + building);
    }


    /**
     * Method from ICleaner
     */
    setTools(toolName: Tool) {
        this._toolName = toolName;
    }

    clean(building: string, roomName: string) {
        console.log("Office cleaner cleans at " + roomName + " in " + building);
    }

    getCleanedRoom(): Array<{[key: string]: string}> {
        return OfficeCleaner._cockroachKilledList;
    }
}
