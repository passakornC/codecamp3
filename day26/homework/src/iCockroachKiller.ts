export interface ICockroachKiller {
    buyBaygon(number: number);
    killCockroach(baygonNumber: number, building: string, roomName: string);
    getTotalKilled(): number;
}
