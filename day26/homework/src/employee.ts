export abstract class Employee {
    private _firstName: string;
    private _lastName: string;
    private _salary: number;
    private _id: string;
    private _dressCode: string;

    constructor();
    constructor(firstname?: string, lastname?: string, salary?: number);
    constructor(firstname?: string, lastname?: string, salary?: number, id?: string) {
        this._firstName = firstname;
        this._lastName = lastname;
        this._salary = salary;
        this._id = id;
    }

    hello() {
        console.log(`Hello ${this._firstName}`);
    }

    abstract work();


    /**
     * Getter
     */
    get firstName(): string {
        return this._firstName;
    }

    get lastName(): string {
        return this._lastName;
    }

    get salary(): number {
        return this._salary;
    }

    get id(): string {
        return this._id;
    }

    get dressCode(): string {
        return this._dressCode;
    }

    /**
     * Setter
     */
    set firstName(value: string) {
        this._firstName = value;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    set salary(value: number) {
        this._salary = value;
    }

    set id(value: string) {
        this._id = value;
    }

    set dressCode(value: string) {
        this._dressCode = value;
    }


}
