export interface ICleaner {
    setTools(toolName: Tool);
    clean(building: string, roomName: string);
    getCleanedRoom(): Array<{[key :string]: string}>;
}
