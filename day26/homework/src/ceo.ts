import {Employee} from "./employee";
import {ICockroachKiller} from "./iCockroachKiller";
import {ICleaner} from "./iCleaner";

export class Ceo extends Employee {
    /**
     * Property
     */
    private employees: Array<Employee>;


    /**
     * Constructor
     */
    constructor(firstName: string, lastName: string);

    constructor(firstName: string, lastName: string, salary?: number); // salary? -> optional

    constructor(firstName: string, lastName: string, salary: number = 1000.00, id?: string, dressCode?: string) {
        super(firstName, lastName, salary);
        this.id = id;
        this.dressCode = dressCode;
    }

    getSalary(): number {
        return this.salary * 2;
    }

    hello() {
        console.log(`Hi, nice to meet you. + ${this.firstName} !`);
    }

    orderKillCockroach(cockroachKiller: ICockroachKiller) {
        cockroachKiller.killCockroach(250, "Building A", "Room A");
    }

    orderCleaned(cleaner: ICleaner) {
        cleaner.clean("Bulding Z", "Room Z");
    }

    addEmployee(employee: Employee) {
        this.employees.push(employee);
    }

    getEmployees(): Array<Employee> {
        return this.employees;
    }

    fire() {
        console.log("fire()");
    }

    hire() {
        console.log("hire()");
    }

    seminar() {
        console.log("seminar()");
    }

    golf () {
        console.log("golf");
    }

    work() {
        this.seminar();
        this.hire();
        this.fire();
        this.golf();
    }
}
