//package com.codecamp.day11;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import java.io.IOException;
//import java.io.File;
//import java.util.ArrayList;
//
///**
// * Hello world!
// */
//public class Homework11_1 {
//    public static void main(String[] args) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        String pathname = "/Users/passakornchoosuk/CodeCamp3/codecamp3/day11_exception/src/main/java/data/data.json";
//
//        try {
//            ArrayList<Employee> employeeList = objectMapper.readValue(new File(pathname), new TypeReference<ArrayList<Employee>>(){});
//
//            for (Employee employee : employeeList) {
//                System.out.printf("id: %d\nfirstname: %s\nlastname: %s\ncompany: %s\nsalary: %d\n", employee.getId(), employee.getFirstname(), employee.getLastname(), employee.getCompany(), employee.getSalary());
//                System.out.println("------------------------------------------------------------");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//}
