package com.codecamp.day11;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.TypeBindings;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Homework11_2 {

    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        String pathname = "https://jsonplaceholder.typicode.com/posts";

        //        try {
        //            JsonNode jsonNode = objectMapper.readTree(new URL(pathname));
        //
        //            for (JsonNode node : jsonNode) {
        //                System.out.printf("userId: %d\nid: %d\ntitle: %s\nbody: %s\n", node.get("userId").intValue(), node.get("id").intValue(), node.get("title").textValue(), node.get("body").textValue());
        //                System.out.println("--------------------------------------------------------------------------------");
        //            }
        //        } catch (IOException e) {
        //            e.printStackTrace();
        //        }
        //
        //        System.out.println("==========++++++++++==========++++++++++==========++++++++++==========++++++++++==========++++++++++");
        try {
            //            ArrayList<Post> postList = objectMapper.readValue(new URL(pathname), new ArrayType<Post>(){});
            ArrayList<Post> postList = objectMapper.readValue(new URL(pathname), new TypeReference<ArrayList<Post>>() {
            });

            for (Post post : postList) {
                System.out.printf("userId: %d\nid: %d\ntitle: %s\nbody: %s\n", post.getUserId(), post.getId(), post.getTitle(), post.getBody());
                System.out.println("--------------------------------------------------------------------------------");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
