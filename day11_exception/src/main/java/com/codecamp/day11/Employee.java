package com.codecamp.day11;

public class Employee {
    private String firstname;
    private String lastname;
    private int salary;
    private int id;
    private String company;

    public Employee () {
    }
    public Employee (String firstname, String lastname, int salary, int id) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastname() {
        return this.lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }

}
